# INTRODUCTION
--------------

This repository contains codality tasks upto lesson 7. 

# RUNNING TASKS
-----------------

Run the following code to build and run docker:

```bash
./buildDocker.sh
./runDocker.sh
```

In order to run the tasks:

1. Go into the folder containing the task
2. Enter the following commands:

```bash
mkdir build
cd build
cmake ..
make
```

Three executables will be made for each task. The placement of these executables will be as follows:
1. app will be in the build directory
2. unit-test in build/tests
3. bench in build/benchmarks




