#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <string>
#include <algorithm>
using myStringType = std::string;

namespace CodilityTaskTDD
{

template <int32_t T = 1>
class StrSymmetryPoint
{
public:
    int solution(myStringType &inputStr)
    {
        
        int N = inputStr.size();
        if (N % 2 == 0)
        {
            return -1;
        }
        else
        {
            if (N == 1)
            {
                return 0;
            }
            int mid = N/2;
            myStringType leftHalf=inputStr.substr(0,mid),rightHalf=inputStr.substr(mid+1,N);
            reverse(rightHalf.begin(),rightHalf.end());
            if(leftHalf==rightHalf)
            {return mid;}
            else
            {
                return -1;
            }
            

        }
    }
};

} // namespace CodilityTaskTDD
#endif
