#include "solutions.h"
#include "gtest/gtest.h"

int symmetryIndex(myStringType input_string);

class StrSymmetryAssert
{
  public:
  StrSymmetryAssert()=delete;
  explicit StrSymmetryAssert(myStringType S) :
    strToFindSymmetry(S){}
  void isSymmetricAtIndex(int expectedSymmetricIndex)
  {
    ASSERT_EQ(expectedSymmetricIndex, symmetryIndex(strToFindSymmetry));
  }
  private:
  myStringType strToFindSymmetry;

};

StrSymmetryAssert assertThat(myStringType InputStr)
{
  StrSymmetryAssert assert {InputStr};
  return assert;
}

int symmetryIndex(myStringType input_string)
{
  CodilityTaskTDD::StrSymmetryPoint<1> questions;
  return questions.solution(input_string);
}

TEST(SolutionTest, emptyStrTest)
{
  assertThat("").isSymmetricAtIndex(-1);
}

TEST(SolutionTest, stringOfLengthOne)
{
  assertThat("B").isSymmetricAtIndex(0);
}

TEST(SolutionTest, stringOfLengthTwo)
{
  assertThat("AB").isSymmetricAtIndex(-1);
}

TEST(SolutionTest, stringOfLengthThree)
{
  assertThat("ABA").isSymmetricAtIndex(1);
}

TEST(SolutionTest, stringOfLengthfour)
{
  assertThat("ABAB").isSymmetricAtIndex(-1);
}


TEST(SolutionTest, largeNonSymmetricString)
{
  myStringType inputstring= "";
  for (int i=0;i<1000;i++)
  {inputstring=inputstring+"ABCDEFGHIJKLMNOP";}


  assertThat(inputstring).isSymmetricAtIndex(-1);
}

TEST(SolutionTest, largeSymmetricString)
{
  myStringType inputstring= "";
  for (int i=0;i<2000;i++)
  {inputstring=inputstring+"ABABABABAB";}
  inputstring=inputstring+"A";

  assertThat(inputstring).isSymmetricAtIndex(10000);
}


TEST(SolutionTest, extremeSizeNonSymmetricString)
{
  myStringType inputstring= "";
  for (int i=0;i<60000;i++)
  {inputstring=inputstring+"ABCDEFGHIJKLMNOP";}           //len=960000


  assertThat(inputstring).isSymmetricAtIndex(-1);
}

TEST(SolutionTest, extremeSizeSymmetricString)
{
  myStringType inputstring= "";
  for (int i=0;i<96000;i++)
  {inputstring=inputstring+"ABABABABAB";}
  inputstring=inputstring+"A";                            //len=960001

  assertThat(inputstring).isSymmetricAtIndex(480000);
}
