#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <vector>
#include <algorithm>

using Input = std::vector<int>;

namespace Codility_TDD
{
template <int32_t T = 1>
class Dominator
{
public:
    int solution(const Input &input_array)
    {
        auto array_size = input_array.size();
        uint32_t no_of_occurrences = 0;
        int dominator_candidate;
        uint32_t candidate_occurrences = 0;

        for (Input::size_type index = 0; index < array_size; index++)
        {
            if (no_of_occurrences == 0)
            {
                dominator_candidate = input_array[index];
                no_of_occurrences++;
            }
            else
            {
                if (input_array[index] != dominator_candidate)
                {
                    no_of_occurrences--;
                }
                else
                {
                    no_of_occurrences++;
                }
            }
        }

        for (Input::size_type index = 0; index < array_size; index++)
        {
            if (input_array[index] == dominator_candidate)
            {
                candidate_occurrences++;
            }
        }

        if (candidate_occurrences > array_size / 2)
        {
            return std::find(input_array.begin(), input_array.end(), dominator_candidate) - input_array.begin();
        }
        else
        {
            return -1;
        }
    }
};

} // namespace Codility_TDD
#endif
