#include "solutions.h"
#include "gtest/gtest.h"

int index_of_dominator(Input &input_array)
{
  Codility_TDD::Dominator<1> question;

  return question.solution(input_array);
}

class dominator_assert
{
public:
  dominator_assert() = delete;
  explicit dominator_assert(const Input &input_array) : find_dominator_in(input_array) {}
  void find_dominator_index_at(int expected_value)
  {
    ASSERT_EQ(index_of_dominator(find_dominator_in), expected_value);
  }

private:
  Input find_dominator_in;
};

dominator_assert assertThat(Input input_array)
{
  dominator_assert assert{input_array};
  return assert;
}

TEST(dominator_index, empty_array)
{
  assertThat({}).find_dominator_index_at(-1);
}

TEST(dominator_index, size_1_array)
{
  assertThat({1}).find_dominator_index_at(0);
}

TEST(dominator_index, size_2_array)
{
  assertThat({1, 2}).find_dominator_index_at(-1);
}

TEST(dominator_index, size_3_array)
{
  assertThat({1, 2, 1}).find_dominator_index_at(0);
}

TEST(dominator_index, no_dominator)
{
  assertThat({1, 2, 3}).find_dominator_index_at(-1);
}

class extreme_test : public ::testing::TestWithParam<std::tuple<Input, int>>
{
protected:
  Codility_TDD::Dominator<1> dominator;
};

TEST_P(extreme_test, extreme_test_cases)
{
  ASSERT_EQ(dominator.solution(std::get<0>(GetParam())), std::get<1>(GetParam()));
}

Input without_dominator()
{
  Input input_array(100000, 0);
  int value = -2147483648;
  for (Input::size_type index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index + value;
  }
  return input_array;
}

Input with_dominator()
{
  Input input_array(100000, 0);
  for (Input::size_type index = 0; index < input_array.size(); index++)
  {
    if (index <= input_array.size() / 2 - 2)
    {
      input_array[index] = index;
    }
    else
    {
      input_array[index] = 2147483647;
    }
  }
  return input_array;
}

Input increasing_occurrences()
{
  Input input_array(100000, 0);
  int index_array = 0;
  for (int index = 0; index_array <= 99999; index++)
  {
    for (int iteration = index; iteration >= 0 && index_array <= 99999; iteration--)
    {
      input_array[index_array] = index;
      index_array++;
    }
  }
  return input_array;
}

INSTANTIATE_TEST_SUITE_P(
    dominator_extreme_test,
    extreme_test,
    ::testing::Values(
        std::make_tuple(Input(100000, 1), 0),
        std::make_tuple(Input(100000, 2147483647), 0),
        std::make_tuple(Input(100000, -2147483648), 0),
        std::make_tuple(without_dominator(), -1),
        std::make_tuple(with_dominator(), 49999),
        std::make_tuple(increasing_occurrences(), -1)));