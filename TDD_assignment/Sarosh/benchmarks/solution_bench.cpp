#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

void BM_Solution(benchmark::State &state)
{
  Codility::MinPerimeterRectangle question;

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(state.range()));
  }
}

BENCHMARK(BM_Solution)
    ->Range(1, 1000000000);