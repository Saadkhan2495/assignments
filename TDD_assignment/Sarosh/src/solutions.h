#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cmath>

namespace Codility
{

class MinPerimeterRectangle
{
public:
    int solution(int N)
    {
        int first_side = sqrt(N);
        int second_side = 0;
      
        while (N % first_side > 0)
        {
            first_side--;
        }

        second_side = N / first_side;
        return 2 * (first_side + second_side);
    }
};
} // namespace Codility
#endif
