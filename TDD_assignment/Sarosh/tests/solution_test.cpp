#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

int actual(int input)
{
  Codility::MinPerimeterRectangle obj;
  return obj.solution(input);
}

class min_perimeter
{
public:
  min_perimeter() = delete;
  explicit min_perimeter(int input) : min(input) {}
  void min_perimeter_is(int expected_perimeter) const
  {
    ASSERT_EQ(expected_perimeter, actual(min));
  }

private:
  const unsigned int min;
};

min_perimeter assertThatForArea(int area)
{
  min_perimeter assert{area};
  return assert;
}

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::MinPerimeterRectangle>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TEST(SolutionTest, min_1)
{

  assertThatForArea(1).min_perimeter_is(4);
}

TYPED_TEST(SolutionTest, min_2)
{

  assertThatForArea(2).min_perimeter_is(6);
}

TYPED_TEST(SolutionTest, small_1)
{
  assertThatForArea(3).min_perimeter_is(8);
}

TYPED_TEST(SolutionTest, small_2)
{
  assertThatForArea(10).min_perimeter_is(14);
}

TYPED_TEST(SolutionTest, small_3)
{
  assertThatForArea(50).min_perimeter_is(30);
}

TYPED_TEST(SolutionTest, small_4)
{
  assertThatForArea(100).min_perimeter_is(40);
}

TYPED_TEST(SolutionTest, small_5_prime)
{
  assertThatForArea(101).min_perimeter_is(204);
}

TYPED_TEST(SolutionTest, medium)
{
  assertThatForArea(10110236).min_perimeter_is(5055126);
}

TYPED_TEST(SolutionTest, extreme_max_value)
{
  assertThatForArea(1000000000).min_perimeter_is(126500);
}

TYPED_TEST(SolutionTest, extreme_large_prime)
{
  assertThatForArea(15486451).min_perimeter_is(30972904);
}

TYPED_TEST(SolutionTest, extreme_large_prime_1)
{
  assertThatForArea(982451653).min_perimeter_is(1964903308);
}