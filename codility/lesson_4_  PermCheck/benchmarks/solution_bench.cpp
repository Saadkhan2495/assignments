#include "solutions.h"
#include <benchmark/benchmark.h>
#include <limits>
#include <vector>

template <int32_t S>
void medium_permutation(benchmark::State &state)
{
  Codility::PermCheck<S> question;

  std::vector<int> inputvector = {};
  for (int i = 1; i < 10000; i++)
    inputvector.push_back(i);
  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium_permutation, 1);
BENCHMARK_TEMPLATE(medium_permutation, 2);
BENCHMARK_TEMPLATE(medium_permutation, 6);
BENCHMARK_TEMPLATE(medium_permutation, 11);
BENCHMARK_TEMPLATE(medium_permutation, 16);

template <int32_t S>
void large_not_permutation(benchmark::State &state)
{
  Codility::PermCheck<S> question;

  std::vector<int> inputvector = {};
  inputvector.push_back(20);
  inputvector.push_back(20);
  for (int i = 1; i < 100000; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_not_permutation, 1);
BENCHMARK_TEMPLATE(large_not_permutation, 2);
BENCHMARK_TEMPLATE(large_not_permutation, 6);
BENCHMARK_TEMPLATE(large_not_permutation, 11);
BENCHMARK_TEMPLATE(large_not_permutation, 16);

template <int32_t S>
void large_range(benchmark::State &state)
{
  Codility::PermCheck<S> question;

  std::vector<int> inputvector = {};
  for (int i = 1; i < 99000; i++)
    inputvector.push_back(i);
  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_range, 1);
BENCHMARK_TEMPLATE(large_range, 2);
BENCHMARK_TEMPLATE(large_range, 6);
BENCHMARK_TEMPLATE(large_range, 11);
BENCHMARK_TEMPLATE(large_range, 16);

template <int32_t S>
void extreme_values(benchmark::State &state)
{
  Codility::PermCheck<S> question;

  std::vector<int> inputvector(80000, 2000);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(extreme_values, 1);
BENCHMARK_TEMPLATE(extreme_values, 2);
BENCHMARK_TEMPLATE(extreme_values, 6);
BENCHMARK_TEMPLATE(extreme_values, 11);
BENCHMARK_TEMPLATE(extreme_values, 16);

template <int32_t S>
void various_permutation(benchmark::State &state)
{
  Codility::PermCheck<S> question;

  std::vector<int> inputvector = {1, 2, 3, 4, 5};

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(various_permutation, 1);
BENCHMARK_TEMPLATE(various_permutation, 2);
BENCHMARK_TEMPLATE(various_permutation, 6);
BENCHMARK_TEMPLATE(various_permutation, 11);
BENCHMARK_TEMPLATE(various_permutation, 16);