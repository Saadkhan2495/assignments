#include "solutions.h"
#include <vector>
#include <iostream>

using namespace std;
int main(void)
{
  int result = -1;
  vector<int> input = {3, 1, 2, 4};
  Codility::PermCheck<1> sundas1;
  Codility::PermCheck<2> sundas2;
  Codility::PermCheck<6> sarosh;
  Codility::PermCheck<11> saad;
  Codility::PermCheck<16> bilal;

  result = sundas1.solution(input);
  std::cout << "sundas1 result: " << result << std::endl;

  result = sundas2.solution(input);
  std::cout << "sundas1 result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;

  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;

  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  //the Answere should be 1

  return 0;
}