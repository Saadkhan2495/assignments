#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <algorithm>
#include <vector>
#include <numeric>

namespace Codility
{

template <int32_t S = 1>
class PermCheck
{
public:
    int solution(std::vector<int> &A)
    {

        sort(A.begin(), A.end());
        int N = A.size();
        int isPerm = 1;
        for (int i = 0; i < N; i++)
        {
            if (A[i] != (i + 1))
            {
                isPerm = 0;
            }
        }

        return isPerm;
    }
};
template <>
class PermCheck<2>
{
    //not correct for some test cases in which sum is same
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        long sum_expected = ((N) * (N + 1)) / 2;
        long sum_actual = accumulate(A.begin(), A.end(), 0);
        int diff = sum_expected - sum_actual;
        if (diff != 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
};

template <>
class PermCheck<6>
{
    //not correct for some test cases in which sum is same
public:
    int solution(std::vector<int> &A)
    {
        sort(A.begin(), A.end());
        for (int i = 0; i < int(A.size()); i++)
            if (A[i] != i + 1)
                return 0;

        return 1;
    }
};

template <>
class PermCheck<11>
{
public:
    int solution(std::vector<int> &input_array)
    {
        uint32_t array_size = input_array.size();
        std::vector<bool> number_exists((array_size + 1), false);
        for (auto const &input_array_value : input_array)
        {
            if (input_array_value > int(array_size) || number_exists[input_array_value])
            {
                return 0;
            }
            number_exists[input_array_value] = true;
        }
        return 1;
    }
};

template <>
class PermCheck<16>
{
public:
    int solution(std::vector<int> &A)
    {
        sort(A.begin(), A.end());

        for (int i = 0; i < int(A.size()); i++)
        {
            if (A[i] != i + 1)
            {
                return 0;
            }
        }
        return 1;
    }
};

} // namespace Codility
#endif
