#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::PermCheck<1>, Codility::PermCheck<2>, Codility::PermCheck<6>, Codility::PermCheck<11>, Codility::PermCheck<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::vector<int> inputArray = {4, 1, 3, 2};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, example2)
{
  // Arrange
  std::vector<int> inputArray = {4, 1, 3};
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_min_max)
{
  // Arrange
  std::vector<int> inputArray = {1};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);

  // Arrange
  std::vector<int> inputArray1 = {1000000000};
  expected = 0;
  actual = -1;
  // Act
  actual = this->question.solution(inputArray1);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, single)
{
  // Arrange
  std::vector<int> inputArray = {2};
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, Double)
{
  // Arrange
  std::vector<int> inputArray = {2, 1};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, antiSum1)
{
  // Arrange
  std::vector<int> inputArray = {1, 2, 3, 5};
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_permutation)
{
  // Arrange
  std::vector<int> inputArray = {};
  inputArray.push_back(2);
  for (int i = 1; i < 100; i++)
    inputArray.push_back(i);
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, permutation_of_ranges)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = 2; i < 100; i++)
    inputArray.push_back(i);
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);
  // Assert
  EXPECT_EQ(actual, expected);
}

//Performance tests

TYPED_TEST(SolutionTest, medium_permutation)
{
  // Arrange
  std::vector<int> inputArray = {};
  inputArray.push_back(2);
  for (int i = 1; i < 10000; i++)
    inputArray.push_back(i);
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_not_permutation)
{
  // Arrange
  std::vector<int> inputArray = {};
  inputArray.push_back(20);
  inputArray.push_back(20);
  for (int i = 1; i < 100000; i++)
    inputArray.push_back(i);
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_range)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = 1; i < 99000; i++)
    inputArray.push_back(i);
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_values)
{
  // Arrange
  std::vector<int> inputArray(80000, 2000);
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, various_permutation)
{
  // Arrange
  std::vector<int> inputArray = {1, 2, 3, 4, 5};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);
  // Assert
  EXPECT_EQ(actual, expected);

  std::vector<int> inputArray1{1, 2, 3, 4, 5, 6, 7, 8, 9};
  expected = 1;
  actual = -1;
  // Act
  actual = this->question.solution(inputArray1);
  // Assert
  EXPECT_EQ(actual, expected);

  std::vector<int> inputArray2{1, 2, 3};
  expected = 1;
  actual = -1;
  // Act
  actual = this->question.solution(inputArray2);
  // Assert
  EXPECT_EQ(actual, expected);
}