#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>
#include <vector>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::OddOccurencesInArray<S> question;
  std::vector<int> input;
  for (int i = 0; i < 2001; i = i + 2)
  {
    int r = rand();
    input.push_back(r);
    input.push_back(r);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

template <int32_t S>
void BM_Solution_1(benchmark::State &state)
{
  Codility::OddOccurencesInArray<S> question;
  std::vector<int> input;
  for (int i = 0; i < 100003; i = i + 2)
  {
    int r = rand();
    input.push_back(r);
    input.push_back(r);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

template <int32_t S>
void BM_Solution_2(benchmark::State &state)
{
  Codility::OddOccurencesInArray<S> question;
  std::vector<int> input;
  for (int i = 0; i < 999999; i = i + 2)
  {
    input.push_back(1);
    input.push_back(2);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

template <int32_t S>
void BM_Solution_3(benchmark::State &state)
{
  Codility::OddOccurencesInArray<S> question;
  std::vector<int> input;
  for (int i = 0; i < 999999; i = i + 2)
  {
    int r = rand();
    input.push_back(r);
    input.push_back(r);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
BENCHMARK_TEMPLATE(BM_Solution_1, 1);
BENCHMARK_TEMPLATE(BM_Solution_1, 6);
BENCHMARK_TEMPLATE(BM_Solution_1, 11);
BENCHMARK_TEMPLATE(BM_Solution_1, 16);
BENCHMARK_TEMPLATE(BM_Solution_2, 1);
BENCHMARK_TEMPLATE(BM_Solution_2, 6);
BENCHMARK_TEMPLATE(BM_Solution_2, 11);
BENCHMARK_TEMPLATE(BM_Solution_2, 16);
BENCHMARK_TEMPLATE(BM_Solution_3, 1);
BENCHMARK_TEMPLATE(BM_Solution_3, 6);
BENCHMARK_TEMPLATE(BM_Solution_3, 11);
BENCHMARK_TEMPLATE(BM_Solution_3, 16);
