#include "solutions.h"
#include "gtest/gtest.h"
#include <vector>
#include <limits>
#include <utility>
#include <cstdlib>
using namespace std;

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::OddOccurencesInArray<1>, Codility::OddOccurencesInArray<6>, Codility::OddOccurencesInArray<16>, Codility::OddOccurencesInArray<11>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  int actual = -1;
  int expected = 7;
  vector<int> input = {9, 3, 9, 3, 9, 7, 9};
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple1)
{
  // Arrange
  int actual = -1;
  int expected = 963;
  vector<int> input = {1, 2, 1, 2, 963};
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2)
{
  // Arrange
  int actual = -1;
  int expected = 11;
  vector<int> input = {1, 1, 1, 11, 1, 1, 1, 1, 1, 1, 1};
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_single)
{
  // Arrange
  int actual = -1;
  int expected = 42;
  vector<int> input = {42};
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small1)
{
  // Arrange
  int actual = -1;
  int expected = 1000000000;
  vector<int> input = {1000000000};
  for (int i = 1; i < 201; i = i + 2)
  {
    input.push_back(i);
    input.push_back(i);
  }
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small2)
{
  // Arrange
  int actual = -1;
  int expected = 1000000000;
  vector<int> input = {1000000000};
  for (int i = 1; i < 601; i = i + 2)
  {
    input.push_back(i);
    input.push_back(i);
  }
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium1)
{
  // Arrange
  int actual = -1;
  int expected = 2000;
  int random = 0;
  vector<int> input = {2000};
  for (int i = 1; i < 2001; i = i + 2)
  {
    random = rand() % 500;
    input.push_back(random);
    input.push_back(random);
  }

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium2)
{
  // Arrange
  int actual = -1;
  int expected = 41;
  int random = 0;
  vector<int> input = {};
  for (int i = 0; i < 100003; i = i + 2)
  {
    random = rand() % 10;
    input.push_back(random);
    input.push_back(random);
  }
  input[1] = 41;
  input[0] = 41;
  input.push_back(41);

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, big1_multiplerepetitions)
{
  // Arrange
  int actual = -1;
  int expected = 4;
  vector<int> input = {};
  for (int i = 0; i < 999999; i++)
  {
    if (i == 52)
      input.push_back(4);
    else
      input.push_back(1);
  }

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, big2)
{
  // Arrange
  int actual = -1;
  int expected = 2;
  int random = 0;
  vector<int> input = {};
  for (int i = 0; i < 999999; i++)
  {
    random = 10 + rand() % 999999;
    input.push_back(random);
    input.push_back(random);
  }
  input.push_back(2);
  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
