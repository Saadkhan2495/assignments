#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <bits/stdc++.h>
#include <vector>
#include <algorithm>

namespace Codility
{

template <int32_t S = 1>
class OddOccurencesInArray
{
public:
  int solution(std::vector<int> &A)
  {
    int N = A.size();
    int unPaired_num = 0;
    if (N == 1)
    {
      unPaired_num = A.at(0);
    }
    else
    {
      sort(A.begin(), A.end());
      for (int i = 0; i < N; i = i + 2)
      {
        if (i == N - 1 || A.at(i) != A.at(i + 1))
        {
          unPaired_num = A.at(i);
          break;
        }
      }
    }
    return unPaired_num;
  }
};

template <>
class OddOccurencesInArray<11>
{
public:
  int solution(std::vector<int> input_array)
  {
    std::sort(input_array.begin(), input_array.end());
    int num = input_array.size() - 1;
    for (auto index = 0; index < num; index = index + 2)
    {
      if (input_array[index] != input_array[index + 1])
      {
        return input_array[index];
      }
    }
    return input_array[input_array.size() - 1];
  }
};
template <>
class OddOccurencesInArray<16>
{
public:
  int solution(std::vector<int> &A)
  {
    int oddOneOut = 0;
    for (int i = 0; i < int(A.size()); i++)
    {
      oddOneOut = oddOneOut ^ A[i];
    }
    return oddOneOut;
  }
};

template <>
class OddOccurencesInArray<6>
{
public:
  int solution(std::vector<int> &A)
  {
    sort(A.begin(), A.end());
    int last_index = A.size() - 1;
    for (int i = 0; i < last_index; i = i + 2)
    {
      if (A[i] != A[i + 1])
        return A[i];
    }

    return A[last_index]; //
  }
};

} // namespace Codility
#endif
