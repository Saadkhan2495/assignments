
#include "solutions.h"
#include <vector>
#include <iostream>
using namespace std;
int main(void)
{
  int result = -1;
  vector<int> input = {1, 1, 2, 2, 3, 4, 4, 9, 9, 1, 1};
  Codility::OddOccurencesInArray<1> sundas;
  Codility::OddOccurencesInArray<6> sarosh;
  Codility::OddOccurencesInArray<11> saad;
  Codility::OddOccurencesInArray<16> bilal;
  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;

  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;

  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
