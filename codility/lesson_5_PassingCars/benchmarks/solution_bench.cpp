#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::PassingCars<S> question;
  std::vector<int> input;
  for (int i = 0; i < 10000; i = i + 1)
  {
    int r = rand();
    input.push_back(r % 2);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
template <int32_t S>
void BM_Solution_1(benchmark::State &state)
{
  Codility::PassingCars<S> question;
  std::vector<int> input;
  for (int i = 0; i < 100000; i = i + 1)
  {
    int r = rand();
    input.push_back(r % 2);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
template <int32_t S>
void BM_Solution_2(benchmark::State &state)
{
  Codility::PassingCars<S> question;
  std::vector<int> input;
  input.push_back(0);
  input.push_back(0);
  for (int i = 0; i < 100000; i = i + 2)
  {
    input.push_back(i % 2);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

template <int32_t S>
void BM_Solution_3(benchmark::State &state)
{
  Codility::PassingCars<S> question;
  std::vector<int> input;
  for (int i = 0; i < 1000000; i = i + 1)
  {
    input.push_back(0);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 2);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
BENCHMARK_TEMPLATE(BM_Solution_1, 1);
BENCHMARK_TEMPLATE(BM_Solution_1, 2);
BENCHMARK_TEMPLATE(BM_Solution_1, 6);
BENCHMARK_TEMPLATE(BM_Solution_1, 11);
BENCHMARK_TEMPLATE(BM_Solution_1, 16);
BENCHMARK_TEMPLATE(BM_Solution_2, 1);
BENCHMARK_TEMPLATE(BM_Solution_2, 2);
BENCHMARK_TEMPLATE(BM_Solution_2, 6);
BENCHMARK_TEMPLATE(BM_Solution_2, 11);
BENCHMARK_TEMPLATE(BM_Solution_2, 16);
BENCHMARK_TEMPLATE(BM_Solution_3, 1);
BENCHMARK_TEMPLATE(BM_Solution_3, 2);
BENCHMARK_TEMPLATE(BM_Solution_3, 6);
BENCHMARK_TEMPLATE(BM_Solution_3, 11);
BENCHMARK_TEMPLATE(BM_Solution_3, 16);
