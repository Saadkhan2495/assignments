#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::PassingCars<1>, Codility::PassingCars<2>, Codility::PassingCars<6>, Codility::PassingCars<11>, Codility::PassingCars<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::vector<int> input = {0, 1, 0, 1, 1};
  int actual = -1;
  int expected = 5;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, single)
{
  // Arrange
  std::vector<int> input = {0};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, double_elements)
{
  // Arrange
  std::vector<int> input = {0, 1};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple)
{
  // Arrange
  std::vector<int> input = {0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1};
  int actual = -1;
  int expected = 24;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::vector<int> input = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0};
  int actual = -1;
  int expected = 462;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
