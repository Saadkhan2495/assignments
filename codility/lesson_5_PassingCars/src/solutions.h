#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
namespace Codility
{

template <int32_t S = 1>
class PassingCars
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        unsigned long count = 0;
        int incrementVal = 0;
        for (int i = 0; i < N; i++)
        {
            if (A[i] == 0)
            {
                incrementVal++;
            }
            else if (A[i] == 1)
            {
                count = count + incrementVal;
            }
            if (count > 1000000000)
                return -1;
        }
        return count;
    }
};

template <>
class PassingCars<11>
{
public:
    int solution(std::vector<int> &input_array)
    {
        int no_of_pairs = 0;
        int no_of_zeros = 0;
        for (auto const &input_array_value : input_array)
        {
            if (input_array_value == 0)
            {
                no_of_zeros++;
            }
            else
            {
                no_of_pairs = no_of_pairs + no_of_zeros * 1;
            }
            if (no_of_pairs > 1000000000)
            {
                return -1;
            }
        }
        return no_of_pairs;
    }
};

template <>
class PassingCars<2>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        int sum = accumulate(A.begin(), A.end(), 0);
        int count = 0;
        for (int i = 0; i < N; i++)
        {
            if (A[i] == 0)
            {
                count = count + sum;
            }
            else if (A[i] == 1)
            {
                sum = sum - 1;
            }
            if (count > 1000000000)
                return -1;
        }
        return count;
    }
};

template <>
class PassingCars<6>
{
public:
    int solution(std::vector<int> &A)
    {
        int count = 0, i, west = 0, size = A.size();
        for (i = size - 1; i >= 0; i--)
        {
            if (A[i] == 1)
            {
                west++;
            }
            else
            {
                count = count + west;

                if (count > 1000000000)
                    return -1;
            }
        }
        return count;
    }
};

template <>
class PassingCars<16>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size(), carPairCount = 0, multiplyFactor = 0;

        for (int i = 0; i < N; i++)
        {
            //finding 0
            if (A[i] == 0)
            {
                multiplyFactor++;
            }
            else if ((A[i] == 1) && (multiplyFactor != 0))
            {
                carPairCount = carPairCount + multiplyFactor;
                if (carPairCount > 1000000000)
                {
                    return -1;
                }
            }
        }
        return carPairCount;
    }
};

} // namespace Codility
#endif
