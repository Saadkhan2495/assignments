#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>
#include <string>

template <int32_t S>
void large1(benchmark::State &state)
{
  Codility::Nesting<S> question;

  std::string input = "";
  for (int i = 0; i < 1000; i++)
    input = input + "(())(())(())";

  while (state.KeepRunning())
  {
    question.solution(input);
  }
}

BENCHMARK_TEMPLATE(large1, 1);
BENCHMARK_TEMPLATE(large1, 6);
BENCHMARK_TEMPLATE(large1, 11);
BENCHMARK_TEMPLATE(large1, 16);

template <int32_t S>
void large_full_ternary_tree(benchmark::State &state)
{
  Codility::Nesting<S> question;

  std::string input = "";
  for (int i = 0; i < 5000; i++)
    input = input + "(((((())))))(((((())))))(((((())))))";

  while (state.KeepRunning())
  {
    question.solution(input);
  }
}

BENCHMARK_TEMPLATE(large_full_ternary_tree, 1);
BENCHMARK_TEMPLATE(large_full_ternary_tree, 6);
BENCHMARK_TEMPLATE(large_full_ternary_tree, 11);
BENCHMARK_TEMPLATE(large_full_ternary_tree, 16);

template <int32_t S>
void multiple_full_binary_trees(benchmark::State &state)
{
  Codility::Nesting<S> question;

  std::string input = "";
  for (int i = 0; i < 5000; i++)
    input = input + "(((((((((())))))))))(((((((((())))))))))";
  input = input + ")";

  while (state.KeepRunning())
  {
    question.solution(input);
  }
}

BENCHMARK_TEMPLATE(multiple_full_binary_trees, 1);
BENCHMARK_TEMPLATE(multiple_full_binary_trees, 6);
BENCHMARK_TEMPLATE(multiple_full_binary_trees, 11);
BENCHMARK_TEMPLATE(multiple_full_binary_trees, 16);

template <int32_t S>
void broad_tree_with_deep_paths(benchmark::State &state)
{
  Codility::Nesting<S> question;

  std::string input = "";
  for (int i = 0; i < 300; i++)
  {
    for (int j = 0; j < 100; j++)
      input = input + "((((((((((((((((";
    for (int j = 0; j < 100; j++)
      input = input + "))))))))))))))))";
  }

  while (state.KeepRunning())
  {
    question.solution(input);
  }
}

BENCHMARK_TEMPLATE(broad_tree_with_deep_paths, 1);
BENCHMARK_TEMPLATE(broad_tree_with_deep_paths, 6);
BENCHMARK_TEMPLATE(broad_tree_with_deep_paths, 11);
BENCHMARK_TEMPLATE(broad_tree_with_deep_paths, 16);