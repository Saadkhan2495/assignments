#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::Nesting<1>, Codility::Nesting<6>, Codility::Nesting<11>, Codility::Nesting<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::string input = "(()(())())";
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, example2)
{
  // Arrange
  std::string input = "())";
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, negative_match)
{
  // Arrange
  std::string input = "())(";
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, empty)
{
  // Arrange
  std::string input = "";
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple_grouped)
{
  // Arrange
  std::string input = "(()(())()()()()()()())";
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::string input = "(()(())()()()(()))";
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

//Performance tests

TYPED_TEST(SolutionTest, large1)
{
  // Arrange
  std::string input = "";
  for (int i = 0; i < 1000; i++)
    input = input + "(())(())(())";

  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);

  std::string input1 = "";
  for (int i = 0; i < 1000; i++)
    input = input + ")(())(())(())(";
  expected = 0;
  actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_full_ternary_tree)
{
  // Arrange
  std::string input = "";
  for (int i = 0; i < 5000; i++)
    input = input + "(((((())))))(((((())))))(((((())))))";

  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, multiple_full_binary_trees)
{
  // Arrange
  std::string input = "";
  for (int i = 0; i < 5000; i++)
    input = input + "(((((((((())))))))))(((((((((())))))))))";
  input = input + ")";
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, broad_tree_with_deep_paths)
{
  // Arrange
  std::string input = "";
  for (int i = 0; i < 300; i++)
  {
    for (int j = 0; j < 100; j++)
      input = input + "((((((((((((((((";
    for (int j = 0; j < 100; j++)
      input = input + "))))))))))))))))";
  }
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(input);
  // Assert
  EXPECT_EQ(actual, expected);
}