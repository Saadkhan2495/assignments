#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>

#include <string>
#include <stack>

namespace Codility
{

template <int32_t T = 1>
class Nesting
{
public:
    int solution(std::string &str)
    {
        if (str.length() == 0)
        {
            return 1;
        }
        std::stack<char> brackets;
        for (int i = 0; i < (int)str.length(); i++)
        {
            if (str[i] == '(')
            {
                brackets.push(str[i]);
            }
            else
            {
                if (brackets.size() == 0)
                {
                    return 0;
                }
                if (str[i] == ')')
                {
                    if (brackets.top() == '(')
                    {
                        brackets.pop();
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        if (brackets.empty())
        {
            return 1;
        }
        return 0;
    }
};

template <>
class Nesting<16>
{
public:
    int solution(std::string &S)
    {
        int N = S.size();
        std::stack<char> checkStack;
        for (int i = 0; i < N; i++)
        {
            if (S[i] == '(')
            {
                checkStack.push(S[i]);
            }
            else
            {
                if (checkStack.empty())
                    return 0;
                checkStack.pop();
            }
        }
        if (checkStack.empty())
            return 1;
        return 0;
    }
};

template <>
class Nesting<6>
{
public:
    int solution(std::string &S)
    {

        std::stack<char> opening;
        int size = S.length();
        for (int i = 0; i < size; i++)
        {
            if (S[i] == '(')
                opening.push('(');
            else if (opening.empty())
                return 0;
            else
                opening.pop();
        }

        if (opening.empty())
            return 1;
        else
            return 0;
    }
};

template <>
class Nesting<11>
{
public:
    int solution(std::string &input_string)
    {
        std::stack<char> brackets;
        for (uint32_t index = 0; index < input_string.size(); index++)
        {
            if (input_string[index] == '(')
            {
                brackets.push(input_string[index]);
            }
            else if (input_string[index] == ')')
            {
                if (brackets.empty() || brackets.top() != '(')
                {
                    return 0;
                }
                else
                {
                    brackets.pop();
                }
            }
        }
        if (brackets.empty())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
};

} // namespace Codility
#endif
