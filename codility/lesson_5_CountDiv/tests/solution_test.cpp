#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::CountDiv<1>, Codility::CountDiv<16>, Codility::CountDiv<11>, Codility::CountDiv<6>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  int actual = -1;
  int expected = 3;

  // Act
  actual = this->question.solution(6, 11, 2);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple)
{
  // Arrange
  int actual = -1;
  int expected = 20;

  // Act
  actual = this->question.solution(11, 345, 17);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, minimal1)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(0, 0, 11);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, minimal2)
{
  // Arrange
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(1, 1, 11);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_ifempty)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(10, 10, 5);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_ifempty_2)
{
  // Arrange
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(10, 10, 7);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_ifempty_3)
{
  // Arrange
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(10, 10, 20);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, range_end_points)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(0, 0, 1);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, range_end_points_2)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(0, 0, 2000000000);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, range_end_points_3)
{
  // Arrange
  int actual = -1;
  int expected = 2;

  // Act
  actual = this->question.solution(0, 2000000000, 2000000000);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, range_end_points_5)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(2000000000, 2000000000, 1);

  // Assert
  EXPECT_EQ(actual, expected);
}