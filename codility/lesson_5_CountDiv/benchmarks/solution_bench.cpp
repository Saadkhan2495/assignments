#include "solutions.h"
#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S> void BM_Solution(benchmark::State &state) {
  
  int A=state.range(0);
  //int K=state.range(1); 
  Codility::CountDiv<S> question;
  
  for (auto _ : state) {
   ::benchmark::DoNotOptimize(question.solution(state.range(0),state.range(1),state.range(2)));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1)
->Args({100,123000001,2})
->Args({101,123000001,10000})
->Args({0,INT32_MAX,5})
->Args({0,INT32_MAX,INT32_MAX-1})
->Args({0,INT32_MAX,2})
->Args({0,INT32_MAX,INT32_MAX-1000000})
->Args({0,INT32_MAX,56})
->Args({0,INT32_MAX,6});
BENCHMARK_TEMPLATE(BM_Solution, 6)
->Args({100,123000001,2})
->Args({101,123000001,10000})
->Args({0,INT32_MAX,5})
->Args({0,INT32_MAX,INT32_MAX-1})
->Args({0,INT32_MAX,2})
->Args({0,INT32_MAX,INT32_MAX-1000000})
->Args({0,INT32_MAX,56})
->Args({0,INT32_MAX,6});

BENCHMARK_TEMPLATE(BM_Solution, 11)
->Args({100,123000001,2})
->Args({101,123000001,10000})
->Args({0,INT32_MAX,5})
->Args({0,INT32_MAX,INT32_MAX-1})
->Args({0,INT32_MAX,2})
->Args({0,INT32_MAX,INT32_MAX-1000000})
->Args({0,INT32_MAX,56})
->Args({0,INT32_MAX,6});

BENCHMARK_TEMPLATE(BM_Solution, 16)
->Args({100,123000001,2})
->Args({101,123000001,10000})
->Args({0,INT32_MAX,5})
->Args({0,INT32_MAX,INT32_MAX-1})
->Args({0,INT32_MAX,2})
->Args({0,INT32_MAX,INT32_MAX-1000000})
->Args({0,INT32_MAX,56})
->Args({0,INT32_MAX,6});