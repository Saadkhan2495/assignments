#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
namespace Codility
{

template <int32_t S = 1>
class CountDiv
{
public:
    int solution(int A, int B, int K)
    {
        int b = B / K;
        int a = (A > 0 ? (A - 1) / K : 0);
        if (A == 0)
        {
            b++;
        }
        return b - a;
    }
};

template <>
class CountDiv<6>
{
public:
    int solution(int A, int B, int K)
    {

        if (A % K == 0)
        {
            return (B / K) - (A / K) + 1;
        }
        else
        {
            return (B / K) - (A / K);
        }
    }
};

template <>
class CountDiv<16>
{
public:
    int solution(int A, int B, int K)
    {
        if (A % K == 0)
        {
            return (B - A) / K + 1;
        }
        return (B - (A - A % K)) / K;
    }
};

template <>
class CountDiv<11>
{
public:
    int solution(int start, int end, int divisible)
    {
        int count = 0;
        int start_flag = false;
        int end_flag = false;
        int start_value = 0;
        int end_value = 0;
        int start_iteration = start;
        int end_iteration = end;
        for (start_iteration = start; start_iteration <= end; start_iteration++)
        {
            if (start_iteration == 0)
            {
                count++;
            }
            if (divisible >= start_iteration && divisible <= end_iteration)
            {
                start_iteration = divisible;
            }
            if (start_iteration % divisible == 0 && !start_flag && start_iteration >= divisible)
            {
                start_flag = true;
                start_value = start_iteration;
            }
            if (end_iteration % divisible == 0 && !end_flag && end_iteration >= divisible)
            {
                end_flag = true;
                end_value = end_iteration + divisible;
            }
            end_iteration--;
            if (start_flag && end_flag)
            {
                break;
            }
        }
        count = count + ((end_value - start_value) / divisible);
        return count;
    }
};

} // namespace Codility
#endif
