
#include "solutions.h"
#include <vector>
#include <iostream>
int main(void)
{
  int result = -1;

  Codility::CountDiv<1> sundas;
  Codility::CountDiv<6> sarosh;
  Codility::CountDiv<16> bilal;
  Codility::CountDiv<11> saad;

  result = sundas.solution(6, 11, 2);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(6, 11, 2);
  std::cout << "sarosh result: " << result << std::endl;

  result = saad.solution(6, 11, 2);
  std::cout << "saad result: " << result << std::endl;

  result = bilal.solution(6, 11, 2);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
