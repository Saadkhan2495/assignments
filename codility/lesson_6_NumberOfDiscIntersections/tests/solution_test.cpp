#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::NumberOfDiscIntersections<1>, Codility::NumberOfDiscIntersections<6> /*,Codility::PassingCars<11>*/, Codility::NumberOfDiscIntersections<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, simple1)
{
  // Arrange
  std::vector<int> input = {1, 2, 3, 4, 5, 1};
  int actual = -1;
  int expected = 13;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::vector<int> input = {1, 5, 2, 1, 4, 0};
  int actual = -1;
  int expected = 11;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2)
{
  // Arrange
  std::vector<int> input = {0, 1};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple3)
{
  // Arrange
  std::vector<int> input = {0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1};
  int actual = -1;
  int expected = 9;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_small)
{
  // Arrange
  std::vector<int> input = {10};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small1)
{
  // Arrange
  std::vector<int> input = {10, 1000000, 1, 0, 100000, 2, 999};
  int actual = -1;
  int expected = 21;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small2)
{
  // Arrange
  std::vector<int> input = {123, 5, 145, 14, 313, 51, 321, 51, 321, 4514, 3521};
  int actual = -1;
  int expected = 55;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small3)
{
  // Arrange
  std::vector<int> input = {1, 23, 3251, 5, 15, 454, 156, 143, 215, 14, 3251, 521};
  int actual = -1;
  int expected = 66;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, overflow)
{
  // Arrange
  std::vector<int> input = {};
  for (int i = 0; i < 100000; i++)
    input.push_back(i);

  int actual = -1;
  int expected = -1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
