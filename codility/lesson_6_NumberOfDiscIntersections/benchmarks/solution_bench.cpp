#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_Solution_medium1(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 1000; i++)
  {
    inputvector.push_back(rand() % 500);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_medium2(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 10000; i++)
  {
    inputvector.push_back(rand() % 500);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_medium3(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 100000; i++)
  {
    inputvector.push_back(rand() % 500);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_medium4(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 99111; i++)
  {
    inputvector.push_back(rand() % 500);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_10M_intersections(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 100000; i++)
  {
    inputvector.push_back(10000000);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_big1(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 100000; i++)
  {
    inputvector.push_back((2000000 + rand()) % 2147483647);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_big2(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 100000; i++)
  {
    inputvector.push_back(rand());
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

template <int32_t S>
void BM_Solution_big3(benchmark::State &state)
{
  Codility::NumberOfDiscIntersections<S> question;
  std::vector<int> inputvector;
  for (int i = 0; i < 100000; i++)
  {
    inputvector.push_back(0);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

BENCHMARK_TEMPLATE(BM_Solution_medium1, 1);
BENCHMARK_TEMPLATE(BM_Solution_medium1, 6);
BENCHMARK_TEMPLATE(BM_Solution_medium1, 16);

BENCHMARK_TEMPLATE(BM_Solution_medium2, 1);
BENCHMARK_TEMPLATE(BM_Solution_medium2, 6);
BENCHMARK_TEMPLATE(BM_Solution_medium2, 16);

BENCHMARK_TEMPLATE(BM_Solution_medium3, 1);
BENCHMARK_TEMPLATE(BM_Solution_medium3, 6);
BENCHMARK_TEMPLATE(BM_Solution_medium3, 16);

BENCHMARK_TEMPLATE(BM_Solution_medium4, 1);
BENCHMARK_TEMPLATE(BM_Solution_medium4, 6);
BENCHMARK_TEMPLATE(BM_Solution_medium4, 16);

BENCHMARK_TEMPLATE(BM_Solution_10M_intersections, 1);
BENCHMARK_TEMPLATE(BM_Solution_10M_intersections, 6);
BENCHMARK_TEMPLATE(BM_Solution_10M_intersections, 16);

BENCHMARK_TEMPLATE(BM_Solution_big1, 1);
BENCHMARK_TEMPLATE(BM_Solution_big1, 6);
BENCHMARK_TEMPLATE(BM_Solution_big1, 16);

BENCHMARK_TEMPLATE(BM_Solution_big2, 1);
BENCHMARK_TEMPLATE(BM_Solution_big2, 6);
BENCHMARK_TEMPLATE(BM_Solution_big2, 16);

BENCHMARK_TEMPLATE(BM_Solution_big3, 1);
BENCHMARK_TEMPLATE(BM_Solution_big3, 6);
BENCHMARK_TEMPLATE(BM_Solution_big3, 16);