#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
#include <algorithm>
namespace Codility
{

template <int32_t S = 1>
class NumberOfDiscIntersections
{
public:
    int solution(std::vector<int> &A)
    {
        std ::vector<int> start = {};
        std ::vector<int> end1 = {};
        int N = A.size(), M = 2147483647;
        for (int i = 0; i < N; i++)
        {
            start.push_back(i - A[i]);
            end1.push_back(i + A[i]);
        }
        sort(start.begin(), start.end());
        sort(end1.begin(), end1.end());
        int count = 0;
        int curr_circles = 0;
        int index1 = 0;
        int index2 = 0;
        while (index1 != (int)start.size() && index2 != (int)end1.size())
        {
            if (index1 < (int)start.size() && start[index1] <= end1[index2])
            {
                count += curr_circles;
                curr_circles += 1;
                index1 += 1;
                if (curr_circles > 10000000)
                {
                    return -1;
                }
            }
            else
            {
                curr_circles -= 1;
                index2 += 1;
            }
        }
        return count;
    }
};

template <>
class NumberOfDiscIntersections<6>
{
public:
    int solution(std::vector<int> &A)
    {

        int n = A.size();
        int total_intersections = 0;
        for (long i = 0; i < n - 1; i++)
            for (long j = i + 1; j < n; j++)
                if (i - A[i] <= j + A[j] && j - A[j] <= i + A[i])
                {
                    if (total_intersections == 10E6)
                        return -1;
                    else
                        total_intersections++;
                }
        return total_intersections;
    }
};

template <>
class NumberOfDiscIntersections<16>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size(), totalDiscIntersections = 0, maxDiscIntersections = 10000000;
        int startDiscIndex = 0, endDiscIndex = 0, discOpened = 0;
        std::vector<int> startDisc = {}, endDisc{};
        //finding the starting and ending points of the disc
        for (int i = 0; i < (N); i++)
        {
            startDisc.push_back(i - A[i]);
            if (2147483647 - A[i] < i)
                endDisc.push_back(A[i]);
            else
                endDisc.push_back(i + A[i]);
        }
        //now sorting to get them is ascending order
        sort(startDisc.begin(), startDisc.end());
        sort(endDisc.begin(), endDisc.end());

        //iterating on all disc
        while (startDiscIndex < N)
        {
            if (startDisc[startDiscIndex] <= endDisc[endDiscIndex])
            {
                //Each newly opened Disc will intersect previously all opened disc
                totalDiscIntersections += discOpened;
                if (totalDiscIntersections > maxDiscIntersections)
                    return -1;
                discOpened++;
                startDiscIndex++;
            }
            else
            {
                discOpened--;
                endDiscIndex++;
            }
        }
        return totalDiscIntersections;
    }
};

} // namespace Codility
#endif
