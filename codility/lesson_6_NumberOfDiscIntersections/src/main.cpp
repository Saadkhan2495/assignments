
#include "solutions.h"
#include <vector>
#include <iostream>
int main(void)
{
  std::vector<int> input = {0, 1, 0, 1, 1};
  int result = -1;

  Codility::NumberOfDiscIntersections<1> sundas;
  Codility::NumberOfDiscIntersections<6> sarosh;
  //Codility::NumberOfDiscIntersections<11> saad;
  Codility::NumberOfDiscIntersections<16> bilal;
  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;
  /*result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;*/
  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
