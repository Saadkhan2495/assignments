#include "solutions.h"
#include "gtest/gtest.h"

#include <climits>
#include <utility>
#include <vector>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::Triangle<6>, Codility::Triangle<1>, Codility::Triangle<11>, Codility::Triangle<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::vector<int> input = {10, 2, 5, 1, 8, 20};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, example2)
{
  // Arrange
  std::vector<int> input = {10, 50, 5, 1};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_empty)
{
  // Arrange
  std::vector<int> input = {};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_single)
{
  // Arrange
  std::vector<int> input = {1};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_twoelements)
{
  // Arrange
  std::vector<int> input = {1, 2};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_negative1)
{
  // Arrange
  std::vector<int> input = {-50, -50, -50};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_arith_overflow1)
{
  // Arrange
  std::vector<int> input = {1, 5, 4, INT_MAX, 8, 3, INT_MAX, 7, 1, INT_MAX};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_arith_overflow2)
{
  // Arrange
  std::vector<int> input = {10, INT_MIN, INT_MIN};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_arith_overflow3)
{
  // Arrange
  std::vector<int> input = {0, INT_MAX, INT_MAX};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, medium1)
{
  // Arrange
  std::vector<int> input;
  for (int i = 0; i < 30; i++)
  {
    input.push_back(rand() % 10000);
  }
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, medium2)
{
  // Arrange
  std::vector<int> input;
  for (int i = 0; i < 50; i++)
  {
    input.push_back(rand() % 1000);
  }
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, medium3)
{
  // Arrange
  std::vector<int> input;
  for (int i = 0; i < 100; i++)
  {
    input.push_back(rand() % 1000);
  }
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
