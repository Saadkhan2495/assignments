#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
#include <algorithm>
namespace Codility
{

template <int32_t S = 1>
class Triangle
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        if (N < 3)
            return 0;

        sort(A.begin(), A.end());

        for (int i = 0; i < N - 2; i++)
        {
            if (A[i + 1] > A[i + 2] - A[i] && A[i + 2] > A[i] - A[i + 1] && A[i] > A[i + 1] - A[i + 2])
            {
                return 1;
            }
        }
        return 0;
    }
};
template <>
class Triangle<6>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        if (N < 3)
            return 0;

        sort(A.begin(), A.end());

        for (int i = 0; i < N - 2; i++)
        {
            if (A[i + 1] > A[i + 2] - A[i] && A[i + 2] > A[i] - A[i + 1] && A[i] > A[i + 1] - A[i + 2])
            {
                return 1;
            }
        }
        return 0;
    }
};

template <>
class Triangle<11>
{
public:
    int solution(std::vector<int> input_array)
    {
        int array_size = input_array.size();
        std::vector<int> sorted_input_array;
        sorted_input_array = input_array;
        std::sort(sorted_input_array.begin(), sorted_input_array.end());
        for (int index = 0; index <= array_size - 3; index++)
        {
            if (sorted_input_array[index] > sorted_input_array[index + 2] - sorted_input_array[index + 1])
            {
                if (sorted_input_array[index + 1] > sorted_input_array[index] - sorted_input_array[index + 2] && sorted_input_array[index] > sorted_input_array[index + 1] - sorted_input_array[index + 2])
                {
                    return 1;
                }
            }
        }
        return 0;
    }
};
template <>
class Triangle<16>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        if (N < 3)
            return 0;
        sort(A.begin(), A.end());
        for (int index = 0; index < N - 2; index++)
        {
            if (long(A[index]) + long(A[index + 1]) > A[index + 2])
                return 1;
        }
        return 0;
    }
};
} // namespace Codility
#endif
