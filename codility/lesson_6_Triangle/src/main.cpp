
#include "solutions.h"
#include <vector>
#include <iostream>
int main(void)
{
  std::vector<int> input = {-50, -50, -50};
  int result = -1;

  Codility::Triangle<1> sundas;
  Codility::Triangle<6> sarosh;
  Codility::Triangle<11> saad;
  Codility::Triangle<16> bilal;
  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;
  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;
  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
