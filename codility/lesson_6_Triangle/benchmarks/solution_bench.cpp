#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>
#include <vector>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::Triangle<S> question;
  std::vector<int> inputvector;

  for (int i = 0; i < 10000; i++)
  {
    int r = rand() % 100000;
    inputvector.push_back(r);
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}
template <int32_t S>
void BM_Solution_1(benchmark::State &state)
{
  Codility::Triangle<S> question;
  std::vector<int> inputvector;

  for (int i = 0; i < 10000; i++)
  {

    inputvector.push_back(i);
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}
template <int32_t S>
void BM_Solution_2(benchmark::State &state)
{
  Codility::Triangle<S> question;
  std::vector<int> inputvector;

  for (int i = 0; i < 100000; i++)
  {
    int r = rand() % 1000000;
    inputvector.push_back(r);
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}
template <int32_t S>
void BM_Solution_3(benchmark::State &state)
{
  Codility::Triangle<S> question;
  std::vector<int> inputvector;

  for (int i = 0; i < 100000; i++)
  {
    int r = rand() % 100000 - 1000001;
    inputvector.push_back(r);
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}
template <int32_t S>
void BM_Solution_4(benchmark::State &state)
{
  Codility::Triangle<S> question;
  std::vector<int> inputvector;

  for (int i = 0; i < 100000; i++)
  {
    int r = rand() % 10 - 11;
    inputvector.push_back(r);
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}
template <int32_t S>
void BM_Solution_5(benchmark::State &state)
{
  Codility::Triangle<S> question;
  std::vector<int> inputvector;

  for (int i = 0; i < 100000; i++)
  {
    inputvector.push_back(-1);
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
BENCHMARK_TEMPLATE(BM_Solution_1, 1);
BENCHMARK_TEMPLATE(BM_Solution_1, 6);
BENCHMARK_TEMPLATE(BM_Solution_1, 11);
BENCHMARK_TEMPLATE(BM_Solution_1, 16);
BENCHMARK_TEMPLATE(BM_Solution_2, 1);
BENCHMARK_TEMPLATE(BM_Solution_2, 6);
BENCHMARK_TEMPLATE(BM_Solution_2, 11);
BENCHMARK_TEMPLATE(BM_Solution_2, 16);
BENCHMARK_TEMPLATE(BM_Solution_3, 1);
BENCHMARK_TEMPLATE(BM_Solution_3, 6);
BENCHMARK_TEMPLATE(BM_Solution_3, 11);
BENCHMARK_TEMPLATE(BM_Solution_3, 16);
BENCHMARK_TEMPLATE(BM_Solution_4, 1);
BENCHMARK_TEMPLATE(BM_Solution_4, 6);
BENCHMARK_TEMPLATE(BM_Solution_4, 11);
BENCHMARK_TEMPLATE(BM_Solution_4, 16);
BENCHMARK_TEMPLATE(BM_Solution_5, 1);
BENCHMARK_TEMPLATE(BM_Solution_5, 6);
BENCHMARK_TEMPLATE(BM_Solution_5, 11);
BENCHMARK_TEMPLATE(BM_Solution_5, 16);
