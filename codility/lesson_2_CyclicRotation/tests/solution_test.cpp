#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::CyclicRotation<1>, Codility::CyclicRotation<6>, Codility::CyclicRotation<16>, Codility::CyclicRotation<11>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::vector<int> actualarray = {3, 8, 9, 7, 6};
  int shift = 3;
  std::vector<int> expected = {9, 7, 6, 3, 8};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, example2)
{
  // Arrange
  std::vector<int> actualarray = {0, 0, 0};
  int shift = 1;
  std::vector<int> expected = {0, 0, 0};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, example3)
{
  // Arrange
  std::vector<int> actualarray = {1, 2, 3, 4};
  int shift = 4;
  std::vector<int> expected = {1, 2, 3, 4};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}
TYPED_TEST(SolutionTest, extreme_empty)
{
  // Arrange
  std::vector<int> actualarray = {};
  int shift = 4;
  std::vector<int> expected = {};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, single)
{
  // Arrange
  std::vector<int> actualarray = {1};
  int shift = 4;
  std::vector<int> expected = {1};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, double_element)
{
  // Arrange
  std::vector<int> actualarray = {1, 2};
  int shift = 2;
  std::vector<int> expected = {1, 2};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, small1)
{
  // Arrange
  std::vector<int> actualarray = {1, 2, 3, 4, 5};
  int shift = 1;
  std::vector<int> expected = {5, 1, 2, 3, 4};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, small2)
{
  // Arrange
  std::vector<int> actualarray = {1, 2, 3, 4, 5};
  int shift = 6;
  std::vector<int> expected = {5, 1, 2, 3, 4};

  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::vector<int> actualarray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
  std::vector<int> expected = {15, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
  int shift = 1;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, medium_random)
{
  // Arrange
  std::vector<int> actualarray = {};
  for (int i = 0; i < 100; i++)
    actualarray.push_back(i);

  std::vector<int> expected = actualarray;
  for (int i = 0; i < 10; i++)
  {
    expected.insert(expected.begin(), expected.back());
    expected.pop_back();
  }
  int shift = 10;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);

  for (int i = 0; i < 50; i++)
  {
    expected.insert(expected.begin(), expected.back());
    expected.pop_back();
  }
  shift = 50;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);

  for (int i = 0; i < 99; i++)
  {
    expected.insert(expected.begin(), expected.back());
    expected.pop_back();
  }
  shift = 99;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}

TYPED_TEST(SolutionTest, maximal)
{
  // Arrange
  std::vector<int> actualarray = {};
  for (int i = 0; i < (2 ^ 31); i++)
    actualarray.push_back(i);

  std::vector<int> expected = actualarray;
  for (int i = 0; i < 10; i++)
  {
    expected.insert(expected.begin(), expected.back());
    expected.pop_back();
  }
  int shift = 10;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);

  for (int i = 0; i < (2 ^ 31); i++)
  {
    expected.insert(expected.begin(), expected.back());
    expected.pop_back();
  }
  shift = 2 ^ 31;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);

  for (int i = 0; i < 1; i++)
  {
    expected.insert(expected.begin(), expected.back());
    expected.pop_back();
  }
  shift = 1;
  // Act
  actualarray = this->question.solution(actualarray, shift);

  // Assert
  EXPECT_EQ(actualarray, expected);
}
