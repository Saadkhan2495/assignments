#include "solutions.h"
#include <benchmark/benchmark.h>
#include <limits>
#include <vector>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::CyclicRotation<S> question;

  std::vector<int> inputvector;
  for (int i = 0; i < 1000; i++)
  {
    inputvector.push_back(rand());
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(inputvector, rand() % 1000));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
