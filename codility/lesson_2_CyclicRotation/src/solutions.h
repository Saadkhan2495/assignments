
#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <vector>
#include <cstdint>
#include <algorithm>
namespace Codility
{

template <int32_t S = 1>
class CyclicRotation
{
public:
  std::vector<int> solution(std::vector<int> &A, int K)
  {
    int N = A.size();
    if (N != 0)
    {
      for (int k = 0; k < K; k++)
      {
        int var = A.at(N - 1);
        for (int i = N - 1; i > 0; i--)
        {
          A.at(i) = A.at(i - 1);
        }
        A.at(0) = var;
      }
    }
    return A;
  }
};

template <>
class CyclicRotation<11>
{
public:
  std::vector<int> solution(std::vector<int> &input_array, int rotate_number)
  {
    int next_value = input_array[0];
    int previous_value = input_array[0];
    int temporary_value;
    if (input_array.size() != 0)
    {
      for (uint32_t iteration = rotate_number; iteration > 0; iteration--)
      {
        for (uint32_t index = 0; index < input_array.size() - 1; index++)
        {
          if (index == 0)
          {
            temporary_value = input_array[index];
            input_array[index] = input_array[input_array.size() - 1];
            next_value = input_array[index + 1];
            input_array[index + 1] = temporary_value;
          }
          else
          {
            temporary_value = input_array[index + 1];
            input_array[index + 1] = next_value;
            next_value = temporary_value;
          }
        }
      }
      return input_array;
    }
    else
    {
      return input_array;
    }
  }
};

template <>
class CyclicRotation<6>
{
public:
  std::vector<int> solution(std::vector<int> &A, int K)
  {
    std::vector<int> temp = A;
    int length = A.size();
    for (int i = 0; i < length; i++)
    {
      temp[(i + K) % length] = A[i];
    }
    return temp;
  }
};

template <>
class CyclicRotation<16>
{
public:
  std::vector<int> solution(std::vector<int> &A, int K)
  {
    if (A.size() == 0)
    {
      return A;
    }
    if (K > int(A.size()))
    {
      K = K % A.size();
    }
    std::rotate(A.begin(), A.begin() + A.size() - K, A.end());
    return A;
  }
};

} // namespace Codility
#endif
