
#include "solutions.h"
#include <vector>
#include <iostream>
using namespace std;

void printvec(std::vector<int> const &input)
{
  int size = input.size();
  for (int i = 0; i < size; i++)
  {
    std::cout << input.at(i) << ' ';
  }
}
int main(void)
{
  vector<int> result, input = {2, 2, 3, 5};
  result.push_back(-1);
  Codility::CyclicRotation<1> sundas;
  Codility::CyclicRotation<6> sarosh;
  Codility::CyclicRotation<11> saad;
  Codility::CyclicRotation<16> bilal;
  result = sundas.solution(input, 2);
  cout << "sundas result: ";
  printvec(result);
  cout << ::endl;
  input = {2, 2, 3, 5};

  result = sarosh.solution(input, 2);
  cout << "sarosh result: ";
  printvec(result);
  cout << ::endl;

  input = {2, 2, 3, 5};
  result = saad.solution(input, 2);
  cout << "saad result: ";
  printvec(result);
  cout << ::endl;

  input = {2, 2, 3, 5};
  result = bilal.solution(input, 2);
  cout << "Bilal result: ";
  printvec(result);
  cout << ::endl;

  return 0;
}
