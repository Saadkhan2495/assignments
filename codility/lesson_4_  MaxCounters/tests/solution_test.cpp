#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::MaxCounters<1>, Codility::MaxCounters<6>, Codility::MaxCounters<11>, Codility::MaxCounters<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  std::vector<int> input_array = {3, 4, 4, 6, 1, 4, 4};
  std::vector<int> expected = {3, 2, 2, 4, 2};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_small)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  std::vector<int> input_array = {6, 6, 6, 6, 6, 6, 6};
  std::vector<int> expected = {0, 0, 0, 0, 0};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, single)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 1;
  std::vector<int> input_array = {1, 1, 2};
  std::vector<int> expected = {2};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random1)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  std::vector<int> input_array = {1, 3, 3, 6, 2, 1, 1, 6, 3, 6, 4, 5, 6, 5, 5, 2, 6, 2, 3, 3, 6};
  std::vector<int> expected = {10, 10, 10, 10, 10};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random2)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  std::vector<int> input_array = {1, 4, 4, 6, 2, 3, 3, 6, 5, 2, 2, 6, 6, 3, 1, 1, 6, 1, 2, 5, 5, 6, 3, 6, 1, 1, 6, 4, 4, 6, 2, 3, 6};
  std::vector<int> expected = {16, 16, 16, 16, 16};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium_random1)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  int count = 1;
  std::vector<int> input_array(100, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  std::vector<int> expected = {49, 49, 49, 49, 50};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium_random2)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  int count = 1;
  std::vector<int> input_array(1000, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  std::vector<int> expected = {499, 499, 499, 499, 500};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_random1)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  int count = 1;
  std::vector<int> input_array(4240, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  std::vector<int> expected = {2119, 2119, 2119, 2119, 2120};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_random2)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  int count = 1;
  std::vector<int> input_array(20000, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  std::vector<int> expected = {9999, 9999, 9999, 9999, 10000};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_large)
{
  // Arrange
  std::vector<int> actual = {};
  int no_of_counters = 5;
  std::vector<int> input_array(100000, 6);
  std::vector<int> expected = {0, 0, 0, 0, 0};

  // Act
  actual = this->question.solution(no_of_counters, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}