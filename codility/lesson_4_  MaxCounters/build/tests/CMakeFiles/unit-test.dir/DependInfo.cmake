# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/emumba/BinaryGap/codility/BinaryGap/tests/solution_test.cpp" "/home/emumba/BinaryGap/codility/BinaryGap/build/tests/CMakeFiles/unit-test.dir/solution_test.cpp.o"
  "/home/emumba/BinaryGap/codility/BinaryGap/tests/test.cpp" "/home/emumba/BinaryGap/codility/BinaryGap/build/tests/CMakeFiles/unit-test.dir/test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "external/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
