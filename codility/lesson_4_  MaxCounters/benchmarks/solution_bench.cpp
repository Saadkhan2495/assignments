#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_medium_random1(benchmark::State &state)
{
  Codility::MaxCounters<S> question;
  std::vector<int> input_array(100, 0);
  int no_of_counters = 5;
  int count = 1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(no_of_counters, input_array));
  }
}

template <int32_t S>
void BM_medium_random2(benchmark::State &state)
{
  Codility::MaxCounters<S> question;
  std::vector<int> input_array(1000, 0);
  int no_of_counters = 5;
  int count = 1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(no_of_counters, input_array));
  }
}

template <int32_t S>
void BM_large_random1(benchmark::State &state)
{
  Codility::MaxCounters<S> question;
  std::vector<int> input_array(4240, 0);
  int no_of_counters = 5;
  int count = 1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(no_of_counters, input_array));
  }
}

template <int32_t S>
void BM_large_random2(benchmark::State &state)
{
  Codility::MaxCounters<S> question;
  std::vector<int> input_array(20000, 0);
  int no_of_counters = 5;
  int count = 1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index % 2 == 0)
    {
      input_array[index] = 6;
    }
    else
    {
      input_array[index] = count;
      count++;
      if (count >= 6)
      {
        count = 1;
      }
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(no_of_counters, input_array));
  }
}

template <int32_t S>
void BM_extreme_large(benchmark::State &state)
{
  Codility::MaxCounters<S> question;
  std::vector<int> input_array(100000, 6);
  int no_of_counters = 5;
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(no_of_counters, input_array));
  }
}

BENCHMARK_TEMPLATE(BM_medium_random1, 1);
BENCHMARK_TEMPLATE(BM_medium_random1, 6);
BENCHMARK_TEMPLATE(BM_medium_random1, 11);
BENCHMARK_TEMPLATE(BM_medium_random1, 16);

BENCHMARK_TEMPLATE(BM_medium_random2, 1);
BENCHMARK_TEMPLATE(BM_medium_random2, 6);
BENCHMARK_TEMPLATE(BM_medium_random2, 11);
BENCHMARK_TEMPLATE(BM_medium_random2, 16);

BENCHMARK_TEMPLATE(BM_large_random1, 1);
BENCHMARK_TEMPLATE(BM_large_random1, 6);
BENCHMARK_TEMPLATE(BM_large_random1, 11);
BENCHMARK_TEMPLATE(BM_large_random1, 16);

BENCHMARK_TEMPLATE(BM_large_random2, 1);
BENCHMARK_TEMPLATE(BM_large_random2, 6);
BENCHMARK_TEMPLATE(BM_large_random2, 11);
BENCHMARK_TEMPLATE(BM_large_random2, 16);

BENCHMARK_TEMPLATE(BM_extreme_large, 1);
BENCHMARK_TEMPLATE(BM_extreme_large, 6);
BENCHMARK_TEMPLATE(BM_extreme_large, 11);
BENCHMARK_TEMPLATE(BM_extreme_large, 16);