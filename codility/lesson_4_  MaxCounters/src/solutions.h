#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <vector>
#include <algorithm>

namespace Codility
{

template <int32_t S = 1>
class MaxCounters
{
public:
    std::vector<int> solution(int N, std::vector<int> &A)
    {
        std::vector<int> v(N, 0);
        int size = A.size();
        int max = 0;
        for (int i = 0; i < size; i++)
        {
            if (A[i] == N + 1)
            {
                std::fill(v.begin(), v.end(), max);
            }
            else
            {
                v[A[i] - 1] += 1;
                if (v[A[i] - 1] > max)
                {
                    max = v[A[i] - 1];
                }
            }
        }
        return v;
    }
};

template <>
class MaxCounters<6>
{
public:
    std::vector<int> solution(int N, std::vector<int> &A)
    {
        std::vector<int> counters;

        //initialize the array of counters
        for (int i = 0; i < N; i++)
            counters.push_back(0);
        int max1 = 0;
        for (int i = 0; i < int(A.size()); i++)
        {
            if (A[i] == N + 1)
            {
                for (int j = 0; j < N; j++)
                    max1 = std::max(counters[j], max1);

                for (int j = 0; j < N; j++)
                    counters[j] = max1;
            }
            else
            {
                counters[A[i] - 1]++;
            }
        }

        return counters;
    }
};

template <>
class MaxCounters<11>
{
public:
    std::vector<int> solution(int no_of_counters, std::vector<int> &input_array)
    {
        std::vector<int> counters(no_of_counters, 0);
        bool max_counter_called = false;
        bool max_value_updated = false;
        int max_value = 0;
        for (uint32_t index = 0; index < input_array.size(); index++)
        {
            if (input_array[index] <= no_of_counters)
            {
                counters[input_array[index] - 1]++;
                if (counters[input_array[index] - 1] > max_value)
                {
                    max_value = counters[input_array[index] - 1];
                    max_value_updated = true;
                }
                max_counter_called = false;
            }
            else if (!max_counter_called && max_value_updated)
            {
                std::fill(counters.begin(), counters.end(), max_value);
                max_counter_called = true;
                max_value_updated = false;
            }
        }
        return counters;
    }
};

template <>
class MaxCounters<16>
{
public:
    std::vector<int> solution(int N, std::vector<int> &A)
    {
        std::vector<int> result(N, 0);
        int CounterBaseValue = 0, counterMaxValue = 0;

        for (int index = 0; index < int(A.size()); index++)
        {
            if (A[index] < N + 1)
            {
                result[A[index] - 1] = std::max(CounterBaseValue, result[A[index] - 1]) + 1;
                counterMaxValue = std::max(counterMaxValue, result[A[index] - 1]);
            }
            else if (A[index] == (N + 1))
            {
                CounterBaseValue = counterMaxValue;
            }
        }
        // setting the remaining values to CounterBaseValue i.e Highest value asked to assign to all counters
        for (int index1 = 0; index1 < N; index1++)
        {
            if (result[index1] < CounterBaseValue)
            {
                result[index1] = CounterBaseValue;
            }
        }
        return result;
    }
};

} // namespace Codility
#endif
