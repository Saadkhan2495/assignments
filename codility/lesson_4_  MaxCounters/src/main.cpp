
#include "solutions.h"

#include <iostream>

int main(void)
{
  std::vector<int> result = {};
  Codility::MaxCounters<1> sundas;
  Codility::MaxCounters<6> sarosh;
  Codility::MaxCounters<11> saad;
  Codility::MaxCounters<16> bilal;
  std::vector<int> array = {3, 4, 4, 6, 1, 4, 4};
  result = sundas.solution(5, array);
  std::cout << "Sundas result: " << std::endl;
  for (uint32_t index = 0; index < result.size(); index++)
  {
    std::cout << result[index] << ", ";
  }
  std::cout << "" << std::endl;
  result = sarosh.solution(5, array);
  std::cout << "Sarosh result: " << std::endl;
  for (uint32_t index = 0; index < result.size(); index++)
  {
    std::cout << result[index] << ", ";
  }
  std::cout << "" << std::endl;
  result = saad.solution(5, array);
  std::cout << "Saad result: " << std::endl;
  for (uint32_t index = 0; index < result.size(); index++)
  {
    std::cout << result[index] << ", ";
  }
  std::cout << "" << std::endl;
  result = bilal.solution(5, array);
  std::cout << "Bilal result: " << std::endl;
  for (uint32_t index = 0; index < result.size(); index++)
  {
    std::cout << result[index] << ", ";
  }
  std::cout << "" << std::endl;

  return 0;
}
