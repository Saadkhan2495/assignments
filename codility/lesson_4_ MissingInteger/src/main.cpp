
#include "solutions.h"

#include <iostream>

int main(void)
{
  int result = -1;
  Codility::MissingInteger<1> sundas;
  Codility::MissingInteger<6> sarosh;
  Codility::MissingInteger<11> saad;
  Codility::MissingInteger<16> bilal;
  std::vector<int> input_array = {1, 3, 6, 4, 1, 2};
  result = sundas.solution(input_array);
  std::cout << "Sundas Result: " << result << std::endl;

  result = sarosh.solution(input_array);
  std::cout << "Sarosh Result: " << result << std::endl;

  result = saad.solution(input_array);
  std::cout << "Saad Result: " << result << std::endl;

  result = bilal.solution(input_array);
  std::cout << "Bilal result: " << result << std::endl;

  return 0;
}
