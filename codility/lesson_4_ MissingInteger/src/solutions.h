#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <vector>
#include <algorithm>

namespace Codility
{

template <int32_t S = 1>
class MissingInteger
{
public:
    int solution(std::vector<int> &A)
    {

        int N = A.size();
        std::vector<int> counter(N + 1);

        for (int i = 0; i < N; i++)
        {
            if (A[i] > 0 && A[i] <= N + 1)
            {
                counter[A[i] - 1] = 1;
            }
        }

        for (int i = 0; i < N + 1; i++)
        {
            if (!(counter[i]))
                return i + 1;
        }

        return -1;
    }
};

template <>
class MissingInteger<6>
{
public:
    int solution(std::vector<int> &A)
    {
        std::sort(A.begin(), A.end());
        int minValue = 1; // to ignore -ve integers

        if (A[A.size() - 1] > 0)
        {
            for (int i = 0; i < int(A.size()); i++)
            {
                if (A[i] == minValue)
                {
                    minValue = minValue + 1;
                }
                if (A[i] > minValue)
                {
                    break;
                }
            }
        }

        return minValue;
    }
};

template <>
class MissingInteger<11>
{
public:
    int solution(std::vector<int> &input_array)
    {
        std::vector<bool> positive_found_check(1000000, false);
        for (auto const &input_array_value : input_array)
        {
            if (input_array_value > 0 && !positive_found_check[input_array_value])
            {
                positive_found_check[input_array_value] = true;
            }
        }
        for (int index = 1;; index++)
        {
            if (!positive_found_check[index])
            {
                return index;
            }
        }
    }
};

template <>
class MissingInteger<16>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        std::vector<bool> m(N + 1, false);
        for (int index = 0; index < N; index++)
        {
            //bcz in extreme case integers will be b/t 1 and N+1
            if (A[index] > 0 && A[index] <= N + 1)
            {
                m[A[index] - 1] = true;
            }
        }
        for (int i = 0; i < N + 1; i++)
        {
            if (!(m[i]))
            {
                return i + 1;
            }
        }
        return 0;
    }
};

} // namespace Codility
#endif
