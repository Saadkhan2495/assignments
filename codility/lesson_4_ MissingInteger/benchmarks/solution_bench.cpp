#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_medium(benchmark::State &state)
{
  Codility::MissingInteger<S> question;
  std::vector<int> input_array(10005, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index <= 5002)
    {
      input_array[index] = -(index + 1);
    }
    else
    {
      input_array[index] = index - 5002;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_1(benchmark::State &state)
{
  Codility::MissingInteger<S> question;
  std::vector<int> input_array(40000, 0);
  int count_1 = 0;
  int count_2 = 1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index <= 20000)
    {
      input_array[index] = count_1 + 1;
      count_1 = count_1 + 2;
    }
    else
    {
      input_array[index] = count_1 + 1;
      count_2 = count_2 + 2;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_2(benchmark::State &state)
{
  Codility::MissingInteger<S> question;
  std::vector<int> input_array(100000, 0);
  int count_1 = 0;
  int count_2 = 1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index <= 50000)
    {
      input_array[index] = count_1 + 1;
      count_1 = count_1 + 2;
    }
    else
    {
      input_array[index] = count_1 + 1;
      count_2 = count_2 + 2;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_3(benchmark::State &state)
{
  Codility::MissingInteger<S> question;
  std::vector<int> input_array(100000, 0);
  int count_1 = -25000;
  int count_2 = -25001;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index <= 50000)
    {
      input_array[index] = count_1 + 1;
      count_1 = count_1 + 2;
    }
    else
    {
      input_array[index] = count_1 + 1;
      count_2 = count_2 + 2;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

BENCHMARK_TEMPLATE(BM_medium, 1);
BENCHMARK_TEMPLATE(BM_medium, 6);
BENCHMARK_TEMPLATE(BM_medium, 11);
BENCHMARK_TEMPLATE(BM_medium, 16);

BENCHMARK_TEMPLATE(BM_large_1, 1);
BENCHMARK_TEMPLATE(BM_large_1, 6);
BENCHMARK_TEMPLATE(BM_large_1, 11);
BENCHMARK_TEMPLATE(BM_large_1, 16);

BENCHMARK_TEMPLATE(BM_large_2, 1);
BENCHMARK_TEMPLATE(BM_large_2, 6);
BENCHMARK_TEMPLATE(BM_large_2, 11);
BENCHMARK_TEMPLATE(BM_large_2, 16);

BENCHMARK_TEMPLATE(BM_large_3, 1);
BENCHMARK_TEMPLATE(BM_large_3, 6);
BENCHMARK_TEMPLATE(BM_large_3, 11);
BENCHMARK_TEMPLATE(BM_large_3, 16);