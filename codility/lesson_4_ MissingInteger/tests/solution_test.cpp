#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::MissingInteger<1>, Codility::MissingInteger<6>, Codility::MissingInteger<11>, Codility::MissingInteger<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example_1)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array = {1, 3, 6, 4, 1, 2};
  int expected = 5;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, example_2)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array = {1, 2, 3};
  int expected = 4;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, example_3)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array = {-1, -3};
  int expected = 1;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_single)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array = {1};
  int expected = 2;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array = {-10, -1, -2, 1, 6, 4, 2, 1, -8, 3};
  int expected = 5;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_min_max_value)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array = {-1000000, -999999, 999999, -999998, 1000000, -997899, 135975, -555333, 1};
  int expected = 2;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, positive_only)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array(200, 0);
  int count_1 = 1;
  int count_2 = 101;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index <= 100)
    {
      input_array[index] = count_1 + 1;
      count_1 = count_1 + 1;
      if (count_1 > 100)
      {
        count_1 = 0;
      }
    }
    else
    {
      input_array[index] = count_2 + 1;
      count_2 = count_2 + 1;
      if (count_2 > 200)
      {
        count_2 = 100;
      }
    }
  }
  int expected = 201;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, negative_only)
{
  // Arrange
  int actual = -1;
  std::vector<int> input_array(100, 0);
  int count = -1;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = count - 1;
    count = count - 1;
    if (count < -50)
    {
      count = 0;
    }
  }
  int expected = 1;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}