#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::MaxProductOfThree<1>, Codility::MaxProductOfThree<2>, Codility::MaxProductOfThree<6> /*,Codility::MaxProductOfThree<11>*/, Codility::MaxProductOfThree<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> inputArray = {-3, 1, 2, -2, 5, 6};
  int expected = 60;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, one_triple)
{
  // Arrange
  std::vector<int> inputArray = {4, 1, 3};
  int expected = 12;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple1)
{
  // Arrange
  std::vector<int> inputArray = {4, 1, 3, 1};
  int expected = 12;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2)
{
  // Arrange
  std::vector<int> inputArray = {4, 1, 3, 1, 5, 6, 20};
  int expected = 600;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);

  // Arrange
  std::vector<int> inputArray1 = {4, 1, 3, 1, 5, 6, 20, 2, 1, 4};
  expected = 600;
  actual = -1;
  // Act
  actual = this->question.solution(inputArray1);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = 2; i < 100; i++)
    inputArray.push_back(i);
  int expected = 941094;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}
//Performance Tests

TYPED_TEST(SolutionTest, medium_range)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = -1000; i < 1000; i = i + 2)
    inputArray.push_back(i);
  int expected = 996004000;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, medium_random)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int j = 0; j < 10; j++)
    for (int i = -1000; i < 1000; i = i + 2)
      inputArray.push_back(i);
  int expected = 998000000;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_random)
{
  // Arrange
  std::vector<int> inputArray = {};

  for (int i = -1000; i < 500; i++)
    inputArray.push_back(i);
  int expected = 498501000;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_large)
{
  // Arrange
  std::vector<int> inputArray = {};

  for (int i = 0; i < 100000; i++)
    inputArray.push_back(1000);
  int expected = 1000000000;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}