#include "solutions.h"
#include <vector>
#include <benchmark/benchmark.h>
#include <limits>
template <int32_t S>
void medium_range(benchmark::State &state)
{
  Codility::MaxProductOfThree<S> question;
  std::vector<int> inputvector = {};
  for (int i = -1000; i < 1000; i = i + 2)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium_range, 1);
BENCHMARK_TEMPLATE(medium_range, 6);
//BENCHMARK_TEMPLATE(medium_range, 11);
BENCHMARK_TEMPLATE(medium_range, 16);

template <int32_t S>
void medium_random(benchmark::State &state)
{
  Codility::MaxProductOfThree<S> question;
  std::vector<int> inputvector = {};
  for (int j = 0; j < 10; j++)
    for (int i = -1000; i < 1000; i = i + 2)
      inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium_random, 1);
BENCHMARK_TEMPLATE(medium_random, 6);
//BENCHMARK_TEMPLATE(medium_random, 11);
BENCHMARK_TEMPLATE(medium_random, 16);

template <int32_t S>
void large_random(benchmark::State &state)
{
  Codility::MaxProductOfThree<S> question;
  std::vector<int> inputvector = {};
  for (int i = -1000; i < 500; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_random, 1);
BENCHMARK_TEMPLATE(large_random, 6);
//BENCHMARK_TEMPLATE(large_random, 11);
BENCHMARK_TEMPLATE(large_random, 16);

template <int32_t S>
void extreme_large(benchmark::State &state)
{
  Codility::MaxProductOfThree<S> question;
  std::vector<int> inputvector = {};
  for (int i = 0; i < 100000; i++)
    inputvector.push_back(1000);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(extreme_large, 1);
BENCHMARK_TEMPLATE(extreme_large, 6);
//BENCHMARK_TEMPLATE(extreme_large, 11);
BENCHMARK_TEMPLATE(extreme_large, 16);