#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
#include <algorithm>
namespace Codility
{

template <int32_t S = 1>
class MaxProductOfThree
{
public:
    int solution(std::vector<int> &A)
    {
        sort(A.begin(), A.end());
        int N = A.size();
        return std::max(A[N - 1] * A[0] * A[1], A[N - 1] * A[N - 2] * A[N - 3]);
    }
};

template <>
class MaxProductOfThree<6>
{
public:
    int solution(std::vector<int> &A)
    {
        int last = A.size() - 1;
        if (last == 2)
            return A[0] * A[1] * A[2];

        sort(A.begin(), A.end());
        return std::max(A[0] * A[1] * A[last], A[last] * A[last - 1] * A[last - 2]);
    }
};

template <>
class MaxProductOfThree<11>
{
public:
    int solution(std::vector<int> input_array)
    {
        uint32_t array_size = input_array.size();
        std::vector<int> sorted_input_array(array_size, 0);
        int min = 1001;
        int max = -1001;
        //Counting Sort
        for (auto const &input_array_value : input_array)
        {
            if (input_array_value < min)
            {
                min = input_array_value;
            }
            if (input_array_value > max)
            {
                max = input_array_value;
            }
        }
        int count_size;
        if (min == max)
        {
            count_size = abs(min) + 1;
        }
        else
        {
            count_size = max - min + 1;
        }
        std::vector<int> count(count_size, 0);
        if (min < 0)
        {
            for (auto const &input_array_value : input_array)
            {
                count[input_array_value + abs(min)]++;
            }
            int index_sorted = 0;
            for (int index = 0; index < count_size; index++)
            {
                for (int value = 0; value < count[index]; value++)
                {
                    sorted_input_array[index_sorted] = index - abs(min);
                    index_sorted++;
                }
            }
        }
        else
        {
            for (auto const &input_array_value : input_array)
            {
                count[input_array_value]++;
            }
            int index_sorted = 0;
            for (int index = 0; index < count_size; index++)
            {
                for (int value = 0; value < count[index]; value++)
                {
                    sorted_input_array[index_sorted] = index;
                    index_sorted++;
                }
            }
        }
        int product_1;
        int product_2;
        product_1 = sorted_input_array[0] * sorted_input_array[1] * sorted_input_array[array_size - 1];
        product_2 = sorted_input_array[array_size - 1] * sorted_input_array[array_size - 2] * sorted_input_array[array_size - 3];
        if (product_1 > product_2)
        {
            return product_1;
        }
        else
        {
            return product_2;
        }
    }
};

template <>
class MaxProductOfThree<16>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size(), product;
        sort(A.begin(), A.end());
        //if all + or -  ......  product of last 3 will be maximum
        product = A[N - 1] * A[N - 2] * A[N - 3];
        if ((A[N - 1] < 0) || (A[0] > 0))
        {
            return product;
        }
        // for some + & - vales, taking product of largest -ive and the largest +ive... by this we get Max Product
        return std::max((A[0] * A[1] * A[N - 1]), product);
    }
};

} // namespace Codility
#endif
