#include "solutions.h"
#include <vector>
#include <iostream>

using namespace std;
int main(void)
{
  int result = -1;
  vector<int> input = {-3, 1, 2, -2, 5, 6};
  Codility::MaxProductOfThree<1> sundas;
  Codility::MaxProductOfThree<6> sarosh;
  //Codility::MaxProductOfThree<11> saad;
  Codility::MaxProductOfThree<16> bilal;

  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;

  // result = saad.solution(input);
  // std::cout << "saad result: " << result << std::endl;

  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;
  //the Answer should be 60

  return 0;
}