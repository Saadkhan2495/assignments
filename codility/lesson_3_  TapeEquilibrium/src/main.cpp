
#include "solutions.h"
#include <vector>
#include <iostream>
using namespace std;
int main(void)
{
  int result = -1;
  vector<int> input = {3, 1, 2, 4, 3};
  Codility::TapeEquilibrium<1> sundas1;
  Codility::TapeEquilibrium<2> sundas2;
  Codility::TapeEquilibrium<6> sarosh;
  Codility::TapeEquilibrium<11> saad;
  Codility::TapeEquilibrium<16> bilal1;
  Codility::TapeEquilibrium<17> bilal2;
  result = sundas1.solution(input);
  std::cout << "sundas1 result: " << result << std::endl;

  result = sundas2.solution(input);
  std::cout << "sundas1 result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;

  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;

  result = bilal1.solution(input);
  std::cout << "bilal1 result: " << result << std::endl;

  result = bilal2.solution(input);
  std::cout << "bilal2 result: " << result << std::endl;

  //the Answere should be 1

  return 0;
}