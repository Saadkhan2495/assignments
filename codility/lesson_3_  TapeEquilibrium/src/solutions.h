#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <climits>
#include <numeric>
#include <vector>
#include <cmath>
namespace Codility
{

template <int32_t S = 1>
class TapeEquilibrium
{
public:
  int solution(std::vector<int> &A)
  {
    int min = 100000000;
    int N = A.size();
    for (int i = 0; i < N - 1; i++)
    {
      signed int sum_first = accumulate(A.begin(), A.begin() + (i + 1), 0);
      signed int sum_second = accumulate(A.begin() + (i + 1), A.end(), 0);
      int current = std::abs(sum_first - sum_second);
      if (current < min)
      {
        min = current;
      }
    }
    return min;
  }
};

template <>
class TapeEquilibrium<2>
{
public:
  int solution(std::vector<int> &A)
  {
    int min = 100000000;
    int N = A.size();
    int sum = accumulate(A.begin(), A.end(), 0);
    for (int i = 0; i < N - 1; i++)
    {
      int sum_first = accumulate(A.begin(), A.begin() + (i + 1), 0);
      int sum_second = sum - sum_first;
      int current = std::abs(sum_first - sum_second);
      if (current < min)
      {
        min = current;
      }
    }
    return min;
  }
};

template <>
class TapeEquilibrium<11>
{
public:
  int solution(std::vector<int> &input_array)
  {
    uint32_t array_size = input_array.size();
    std::vector<int> sum_left;
    std::vector<int> sum_right;
    sum_left.reserve(array_size - 1);
    sum_right.reserve(array_size - 1);
    int min_difference = -1;
    int difference;
    sum_left[0] = input_array[0];
    sum_right[0] = input_array[array_size - 1];
    for (uint32_t index = 1; index < array_size - 1; index++)
    {
      sum_left[index] = sum_left[index - 1] + input_array[index];
      sum_right[index] = sum_right[index - 1] + input_array[array_size - 1 - index];
    }
    for (uint32_t split = 0; split < array_size - 1; split++)
    {
      difference = std::abs(sum_left[split] - sum_right[array_size - 2 - split]);
      if (min_difference == -1)
      {
        min_difference = difference;
      }
      else if (difference < min_difference)
      {
        min_difference = difference;
      }
    }
    return min_difference;
  }
};

template <>
class TapeEquilibrium<16>
{
public:
  int solution(std::vector<int> &A)
  {
    int LeftSum = 0, minAbsDiff = INT_MAX, diff, N = A.size();
    int Sum = accumulate(A.begin(), A.end(), 0);
    for (int i = 0; i < N - 1; i++)
    {
      LeftSum += A[i];
      // RightSum = Sum - LeftSum
      diff = std::abs(Sum - 2 * LeftSum);
      if (diff <= minAbsDiff)
        minAbsDiff = diff;
    }
    return minAbsDiff;
  }
};

template <>
class TapeEquilibrium<17>
{
public:
  int solution(std::vector<int> &A)
  {
    //less performance methode
    int sum = accumulate(A.begin(), A.end(), 0);
    int absDiff = INT_MAX, diff;
    for (int p = 1; p < int(A.size()); p++)
    {
      int sumL = 0, sumR = 0;
      sumL = accumulate(A.begin(), A.begin() + p, 0);
      sumR = sum - sumL;
      diff = std::abs(sumL - sumR);
      if (absDiff > diff)
      {
        absDiff = diff;
      }
    }
    return absDiff;
  }
};

template <>
class TapeEquilibrium<6>
{
public:
  int solution(std::vector<int> &A)
  {

    int length = A.size();
    if (length == 2)
      return std::abs(A[0] - A[1]);

    long long int sum = 0;
    long int sum_left = 0, sum_right = 0, diff = 0, min_diff = __INT64_MAX__;
    for (int i = 0; i < length; i++)
      sum = sum + A[i];
    for (int i = 0; i < length - 1; i++)
    {
      sum_left = sum_left + A[i];
      sum_right = sum - sum_left;
      diff = std::abs(sum_left - sum_right);
      if (diff < min_diff)
        min_diff = diff;
    }

    return min_diff;
  }
};

} // namespace Codility
#endif
