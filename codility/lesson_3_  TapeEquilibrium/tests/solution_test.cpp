#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::TapeEquilibrium<1>, Codility::TapeEquilibrium<2>, Codility::TapeEquilibrium<6>, Codility::TapeEquilibrium<11>, Codility::TapeEquilibrium<16>, Codility::TapeEquilibrium<17>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> inputArray = {3, 1, 2, 4, 3};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, Double)
{
  // Arrange
  std::vector<int> inputArray = {3, 1};
  int expected = 2;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple_positive)
{
  // Arrange
  std::vector<int> inputArray = {5, 3, 4, 3, 2};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple_negative)
{
  // Arrange
  std::vector<int> inputArray = {-5, -3, -4, -3, -2};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple_boundary)
{
  // Arrange
  std::vector<int> inputArray = {2, 2};
  int expected = 0;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = 0; i < 100; i++)
    inputArray.push_back(i);
  int expected = 20;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = -5; i < 5; i++)
    inputArray.push_back(i);
  int expected = 5;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

//Performance

TYPED_TEST(SolutionTest, medium_random1)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int j = 0; j < 9; j++)
    for (int i = -400; i < 600; i++)
    {
      inputArray.push_back(i);
    }
  int expected = 138;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium_random2)
{
  // Arrange
  std::vector<int> inputArray = {};
  for (int i = 0; i < 9; i++)
  {
    for (int j = -1000; j < 50; j++)
      inputArray.push_back(j);
  }
  int expected = 247;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_ones)
{
  // Arrange
  std::vector<int> inputArray(30001, 1);

  //  for (int j=0;j<30000;j++)
  // {
  //   inputArray.push_back(1);
  //   inputArray.push_back(-1);
  //   inputArray.push_back(0);
  //   }
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_random)
{
  // Arrange
  std::vector<int> inputArray = {};

  for (int j = 0; j < 9001; j++)
  {
    inputArray.push_back(4);
    inputArray.push_back(50);
    inputArray.push_back(140);
  }
  int expected = 86;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_sequence)
{
  // Arrange
  std::vector<int> inputArray = {};

  for (int j = 0; j < 9001; j++)
  {
    inputArray.push_back(4);
    inputArray.push_back(50);
    inputArray.push_back(140);
  }
  int expected = 86;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_extreme)
{
  // Arrange
  std::vector<int> inputArray = {};

  for (int j = 0; j < 9001; j++)
  {
    inputArray.push_back(900);
    inputArray.push_back(-800);
    inputArray.push_back(5);
  }
  int expected = 15;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}