#include "solutions.h"
#include <benchmark/benchmark.h>
#include <limits>
#include <vector>

template <int32_t S>
void medium_random1(benchmark::State &state)
{
  Codility::TapeEquilibrium<S> question;

  std::vector<int> inputvector = {};
  for (int j = 0; j < 9; j++)
    for (int i = -400; i < 600; i++)
    {
      inputvector.push_back(i);
    }

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium_random1, 1);
BENCHMARK_TEMPLATE(medium_random1, 2);
BENCHMARK_TEMPLATE(medium_random1, 6);
BENCHMARK_TEMPLATE(medium_random1, 11);
BENCHMARK_TEMPLATE(medium_random1, 16);
BENCHMARK_TEMPLATE(medium_random1, 17);

template <int32_t S>
void medium_random2(benchmark::State &state)
{
  Codility::TapeEquilibrium<S> question;

  std::vector<int> inputvector = {};
  for (int i = 0; i < 9; i++)
  {
    for (int j = -1000; j < 50; j++)
      inputvector.push_back(j);
  }

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium_random2, 1);
BENCHMARK_TEMPLATE(medium_random2, 2);
BENCHMARK_TEMPLATE(medium_random2, 6);
BENCHMARK_TEMPLATE(medium_random2, 11);
BENCHMARK_TEMPLATE(medium_random2, 16);
BENCHMARK_TEMPLATE(medium_random2, 17);

template <int32_t S>
void large_ones(benchmark::State &state)
{
  Codility::TapeEquilibrium<S> question;

  std::vector<int> inputvector(30001, 1);
  for (int j = 0; j < 9; j++)
    for (int i = -400; i < 600; i++)
    {
      inputvector.push_back(i);
    }

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_ones, 1);
BENCHMARK_TEMPLATE(large_ones, 2);
BENCHMARK_TEMPLATE(large_ones, 6);
BENCHMARK_TEMPLATE(large_ones, 11);
BENCHMARK_TEMPLATE(large_ones, 16);
BENCHMARK_TEMPLATE(large_ones, 17);

template <int32_t S>
void large_random(benchmark::State &state)
{
  Codility::TapeEquilibrium<S> question;

  std::vector<int> inputvector = {};
  for (int j = 0; j < 9001; j++)
  {
    inputvector.push_back(4);
    inputvector.push_back(50);
    inputvector.push_back(140);
  }

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_random, 1);
BENCHMARK_TEMPLATE(large_random, 2);
BENCHMARK_TEMPLATE(large_random, 6);
BENCHMARK_TEMPLATE(large_random, 11);
BENCHMARK_TEMPLATE(large_random, 16);
BENCHMARK_TEMPLATE(large_random, 17);

template <int32_t S>
void large_sequence(benchmark::State &state)
{
  Codility::TapeEquilibrium<S> question;

  std::vector<int> inputvector = {};
  for (int j = 0; j < 9001; j++)
  {
    inputvector.push_back(4);
    inputvector.push_back(50);
    inputvector.push_back(140);
  }
  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_sequence, 1);
BENCHMARK_TEMPLATE(large_sequence, 2);
BENCHMARK_TEMPLATE(large_sequence, 6);
BENCHMARK_TEMPLATE(large_sequence, 11);
BENCHMARK_TEMPLATE(large_sequence, 16);
BENCHMARK_TEMPLATE(large_sequence, 17);

template <int32_t S>
void large_extreme(benchmark::State &state)
{
  Codility::TapeEquilibrium<S> question;

  std::vector<int> inputvector = {};
  for (int j = 0; j < 9001; j++)
  {
    inputvector.push_back(900);
    inputvector.push_back(-800);
    inputvector.push_back(5);
  }

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_extreme, 1);
BENCHMARK_TEMPLATE(large_extreme, 2);
BENCHMARK_TEMPLATE(large_extreme, 6);
BENCHMARK_TEMPLATE(large_extreme, 11);
BENCHMARK_TEMPLATE(large_extreme, 16);
BENCHMARK_TEMPLATE(large_extreme, 17);