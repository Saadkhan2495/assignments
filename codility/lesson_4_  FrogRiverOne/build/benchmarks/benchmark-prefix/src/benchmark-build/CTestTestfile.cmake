# CMake generated Testfile for 
# Source directory: /home/emumba/BinaryGap/codility/BinaryGap/build/benchmarks/benchmark-prefix/src/benchmark
# Build directory: /home/emumba/BinaryGap/codility/BinaryGap/build/benchmarks/benchmark-prefix/src/benchmark-build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("test")
