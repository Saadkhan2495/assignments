# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/emumba/BinaryGap/codility/BinaryGap/benchmarks/benchmark.cpp" "/home/emumba/BinaryGap/codility/BinaryGap/build/benchmarks/CMakeFiles/bench.dir/benchmark.cpp.o"
  "/home/emumba/BinaryGap/codility/BinaryGap/benchmarks/solution_bench.cpp" "/home/emumba/BinaryGap/codility/BinaryGap/build/benchmarks/CMakeFiles/bench.dir/solution_bench.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "external/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
