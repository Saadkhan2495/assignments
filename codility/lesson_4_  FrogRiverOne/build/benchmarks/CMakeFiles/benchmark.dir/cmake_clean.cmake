file(REMOVE_RECURSE
  "CMakeFiles/benchmark"
  "CMakeFiles/benchmark-complete"
  "benchmark-prefix/src/benchmark-stamp/benchmark-install"
  "benchmark-prefix/src/benchmark-stamp/benchmark-mkdir"
  "benchmark-prefix/src/benchmark-stamp/benchmark-download"
  "benchmark-prefix/src/benchmark-stamp/benchmark-update"
  "benchmark-prefix/src/benchmark-stamp/benchmark-patch"
  "benchmark-prefix/src/benchmark-stamp/benchmark-configure"
  "benchmark-prefix/src/benchmark-stamp/benchmark-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/benchmark.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
