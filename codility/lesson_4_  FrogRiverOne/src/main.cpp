
#include "solutions.h"

#include <iostream>

int main(void)
{
  int result = -1;
  Codility::FrogRiverOne<1> sundas;
  Codility::FrogRiverOne<6> sarosh;
  Codility::FrogRiverOne<11> saad;
  Codility::FrogRiverOne<16> bilal;
  std::vector<int> array = {1, 2, 3, 4};
  result = sundas.solution(3, array);
  std::cout << "Sundas result: " << result << std::endl;

  result = sarosh.solution(3, array);
  std::cout << "Sarosh result: " << result << std::endl;

  result = saad.solution(3, array);
  std::cout << "Saad result: " << result << std::endl;

  result = bilal.solution(3, array);
  std::cout << "Bilal result: " << result << std::endl;

  return 0;
}
