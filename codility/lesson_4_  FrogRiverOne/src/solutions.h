#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <algorithm>
#include <vector>
#include <map>
namespace Codility
{

template <int32_t S = 1>
class FrogRiverOne
{
public:
    int solution(int X, std::vector<int> &A)
    {

        int earliestTime = -1;

        std::vector<int> v(X + 1, -1);

        for (int index = 0; index < int(A.size()); ++index)
        {
            if (v[A[index]] == -1)
            {
                v[A[index]] = index;
            }
        }

        for (int index = 1; index <= X; ++index)
        {
            if (v[index] == -1)
            {
                earliestTime = -1;
                break;
            }
            earliestTime = std::max(earliestTime, v[index]);
        }

        return earliestTime;
    }
};

template <>
class FrogRiverOne<11>
{
public:
    int solution(int distance, std::vector<int> &input_array)
    {
        int time = -1;
        int distance_sum = distance;
        int check_sum = 0;
        for (int add = 1; add < distance; add++)
        {
            distance_sum = distance_sum + add;
        }
        std::vector<bool> leave_exist(distance, false);
        for (auto const &input_array_value : input_array)
        {
            if (leave_exist[input_array_value - 1] == false)
            {
                leave_exist[input_array_value - 1] = true;
                check_sum = check_sum + input_array_value;
                time++;
                if (check_sum == distance_sum)
                {
                    return time;
                }
            }
            else
            {
                time++;
            }
        }
        return -1;
    }
};

template <>
class FrogRiverOne<16>
{
public:
    int solution(int X, std::vector<int> &A)
    {
        int N = A.size();
        std::map<int, bool> m;
        // The second key value pair will simply be ignored
        for (int index = 0; index < N; index++)
        {
            m[A[index]] = true;
            if (int(m.size()) == X)
            {
                return index;
            }
        }

        return -1;
    }
};

template <>
class FrogRiverOne<6>
{
public:
    int solution(int X, std::vector<int> &A)
    {
        std::map<int, bool> m;
        int size = A.size();
        for (int i = 0; i < size; i++)
        {
            int element = A[i];
            m[element] = true;
            if (int(m.size()) == X)
            {
                return i;
            }
        }
        return -1;
    }
};

} // namespace Codility
#endif
