#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::FrogRiverOne<1>, Codility::FrogRiverOne<6>, Codility::FrogRiverOne<11>, Codility::FrogRiverOne<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  int actual = -2;
  int distance = 5;
  std::vector<int> input_array = {1, 3, 1, 4, 2, 3, 5, 4};
  int expected = 6;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple)
{
  // Arrange
  int actual = -2;
  int distance = 4;
  std::vector<int> input_array = {1, 2, 2, 3, 2, 1, 1, 4, 2, 1};
  int expected = 7;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, single)
{
  // Arrange
  int actual = -2;
  int distance = 1;
  std::vector<int> input_array = {1};
  int expected = 0;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_frog)
{
  // Arrange
  int actual = -2;
  int distance = 5;
  std::vector<int> input_array = {3, 3, 1, 2, 4, 2, 1, 2};
  int expected = -1;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random1)
{
  // Arrange
  int actual = -2;
  int distance = 50;
  std::vector<int> input_array(100, 0);
  for (uint32_t index = 0; index < 100; index++)
  {
    if (index < 49)
    {
      input_array[index] = index + 1;
    }
    if (index == 49)
    {
      input_array[index] = 1;
    }
    if (index > 49 && index < 99)
    {
      input_array[index] = 99 - index;
    }
    if (index == 99)
    {
      input_array[index] = 50;
    }
  }
  int expected = 99;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random2)
{
  // Arrange
  int actual = -2;
  int distance = 60;
  std::vector<int> input_array(100, 0);
  for (uint32_t index = 0; index < 100; index++)
  {
    if (index < 59)
    {
      input_array[index] = index + 1;
    }
    if (index == 75)
    {
      input_array[index] = 60;
    }
    if (index == 59)
    {
      input_array[index] = 1;
    }
    if (index > 59 && index != 75)
    {
      input_array[index] = 99 - index;
    }
  }
  int expected = 75;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_leaves)
{
  // Arrange
  int actual = -2;
  int distance = 2;
  std::vector<int> input_array(100000, 1);
  int expected = -1;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium_random)
{
  // Arrange
  int actual = -2;
  int distance = 4500;
  std::vector<int> input_array(distance * 2, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 4499)
    {
      input_array[index] = index + 1;
    }
    else if (index == 4499)
    {
      input_array[index] = 11;
    }
    else if (index > 4499 && index != 5656)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 4500;
    }
  }
  int expected = 5656;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium_range)
{
  // Arrange
  int actual = -2;
  int distance = 5000;
  std::vector<int> input_array(distance * 2, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 4999)
    {
      input_array[index] = index + 1;
    }
    else if (index == 4999)
    {
      input_array[index] = 11;
    }
    else if (index > 4999 && index != 7800)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 5000;
    }
  }
  int expected = 7800;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_random)
{
  // Arrange
  int actual = -2;
  int distance = 9500;
  std::vector<int> input_array(distance * 2, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 9499)
    {
      input_array[index] = index + 1;
    }
    else if (index == 9499)
    {
      input_array[index] = 11;
    }
    else if (index > 9499 && index != 15000)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 9500;
    }
  }
  int expected = 15000;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_permutation)
{
  // Arrange
  int actual = -2;
  int distance = 10000;
  std::vector<int> input_array(distance * 2, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 9999)
    {
      input_array[index] = index + 1;
    }
    else if (index == 9999)
    {
      input_array[index] = 11;
    }
    else if (index > 9999 && index != 18000)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 10000;
    }
  }
  int expected = 18000;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_range)
{
  // Arrange
  int actual = -2;
  int distance = 30000;
  std::vector<int> input_array(42000, 1);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index + 1;
  }
  int expected = 29999;

  // Act
  actual = this->question.solution(distance, input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}