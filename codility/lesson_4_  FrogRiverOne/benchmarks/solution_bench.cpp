#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_medium_random(benchmark::State &state)
{
  Codility::FrogRiverOne<S> question;
  std::vector<int> input_array;
  int distance = 4500;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 4499)
    {
      input_array[index] = index + 1;
    }
    else if (index == 4499)
    {
      input_array[index] = 11;
    }
    else if (index > 4499 && index != 5656)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 4500;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(distance, input_array));
  }
}

template <int32_t S>
void BM_medium_range(benchmark::State &state)
{
  Codility::FrogRiverOne<S> question;
  std::vector<int> input_array;
  int distance = 5000;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 4999)
    {
      input_array[index] = index + 1;
    }
    else if (index == 4999)
    {
      input_array[index] = 11;
    }
    else if (index > 4999 && index != 7800)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 5000;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(distance, input_array));
  }
}

template <int32_t S>
void BM_large_random(benchmark::State &state)
{
  Codility::FrogRiverOne<S> question;
  std::vector<int> input_array;
  int distance = 9500;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 9499)
    {
      input_array[index] = index + 1;
    }
    else if (index == 9499)
    {
      input_array[index] = 11;
    }
    else if (index > 9499 && index != 15000)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 9500;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(distance, input_array));
  }
}

template <int32_t S>
void BM_large_permutation(benchmark::State &state)
{
  Codility::FrogRiverOne<S> question;
  std::vector<int> input_array;
  int distance = 10000;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 9999)
    {
      input_array[index] = index + 1;
    }
    else if (index == 9999)
    {
      input_array[index] = 11;
    }
    else if (index > 9999 && index != 18000)
    {
      input_array[index] = input_array.size() - 1 - index;
    }
    else
    {
      input_array[index] = 10000;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(distance, input_array));
  }
}

template <int32_t S>
void BM_large_range(benchmark::State &state)
{
  Codility::FrogRiverOne<S> question;
  std::vector<int> input_array;
  int distance = 30000;
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index + 1;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(distance, input_array));
  }
}

BENCHMARK_TEMPLATE(BM_medium_random, 1);
BENCHMARK_TEMPLATE(BM_medium_random, 6);
BENCHMARK_TEMPLATE(BM_medium_random, 11);
BENCHMARK_TEMPLATE(BM_medium_random, 16);

BENCHMARK_TEMPLATE(BM_medium_range, 1);
BENCHMARK_TEMPLATE(BM_medium_range, 6);
BENCHMARK_TEMPLATE(BM_medium_range, 11);
BENCHMARK_TEMPLATE(BM_medium_range, 16);

BENCHMARK_TEMPLATE(BM_large_random, 1);
BENCHMARK_TEMPLATE(BM_large_random, 6);
BENCHMARK_TEMPLATE(BM_large_random, 11);
BENCHMARK_TEMPLATE(BM_large_random, 16);

BENCHMARK_TEMPLATE(BM_large_permutation, 1);
BENCHMARK_TEMPLATE(BM_large_permutation, 6);
BENCHMARK_TEMPLATE(BM_large_permutation, 11);
BENCHMARK_TEMPLATE(BM_large_permutation, 16);

BENCHMARK_TEMPLATE(BM_large_range, 1);
BENCHMARK_TEMPLATE(BM_large_range, 6);
BENCHMARK_TEMPLATE(BM_large_range, 11);
BENCHMARK_TEMPLATE(BM_large_range, 16);