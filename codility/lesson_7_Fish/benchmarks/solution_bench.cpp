#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_medium_random(benchmark::State &state)
{
  Codility::Fish<S> question;
  std::vector<int> fish_size(5000, 0);
  std::vector<int> fish_direction(5000, 0);
  for (uint32_t index = 0; index < fish_size.size(); index++)
  {
    fish_size[index] = index * 10 + 1;
    if (index % 2 == 0)
    {
      fish_direction[index] = 1;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(fish_size, fish_direction));
  }
}

template <int32_t S>
void BM_large_random(benchmark::State &state)
{
  Codility::Fish<S> question;
  std::vector<int> fish_size(100000, 0);
  std::vector<int> fish_direction(100000, 0);
  for (uint32_t index = 0; index < fish_size.size(); index++)
  {
    fish_size[index] = index * 10 + 1;
    if (index % 2 == 0)
    {
      fish_direction[index] = 1;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(fish_size, fish_direction));
  }
}

template <int32_t S>
void BM_extreme_range1(benchmark::State &state)
{
  Codility::Fish<S> question;
  std::vector<int> fish_size(100000, 0);
  std::vector<int> fish_direction(100000, 0);
  for (uint32_t index = 0; index < fish_size.size(); index++)
  {
    fish_size[index] = index * 10 + 1;
    if (index == 99999)
    {
      fish_direction[index] = 1;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(fish_size, fish_direction));
  }
}

template <int32_t S>
void BM_extreme_range2(benchmark::State &state)
{
  Codility::Fish<S> question;
  std::vector<int> fish_size(100000, 0);
  std::vector<int> fish_direction(100000, 0);
  for (uint32_t index = 0; index < fish_size.size(); index++)
  {
    fish_size[index] = index * 10 + 1;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(fish_size, fish_direction));
  }
}

BENCHMARK_TEMPLATE(BM_medium_random, 1);
BENCHMARK_TEMPLATE(BM_medium_random, 6);
BENCHMARK_TEMPLATE(BM_medium_random, 11);
BENCHMARK_TEMPLATE(BM_medium_random, 16);

BENCHMARK_TEMPLATE(BM_large_random, 1);
BENCHMARK_TEMPLATE(BM_large_random, 6);
BENCHMARK_TEMPLATE(BM_large_random, 11);
BENCHMARK_TEMPLATE(BM_large_random, 16);

BENCHMARK_TEMPLATE(BM_extreme_range1, 1);
BENCHMARK_TEMPLATE(BM_extreme_range1, 6);
BENCHMARK_TEMPLATE(BM_extreme_range1, 11);
BENCHMARK_TEMPLATE(BM_extreme_range1, 16);

BENCHMARK_TEMPLATE(BM_extreme_range2, 1);
BENCHMARK_TEMPLATE(BM_extreme_range2, 6);
BENCHMARK_TEMPLATE(BM_extreme_range2, 11);
BENCHMARK_TEMPLATE(BM_extreme_range2, 16);
