
#include "solutions.h"
#include <vector>
#include <iostream>
int main(void)
{
  std::vector<int> fish_size = {4, 3, 2, 1, 5};
  std::vector<int> fish_direction = {0, 1, 0, 0, 0};
  int result = -1;

  Codility::Fish<1> sundas;
  Codility::Fish<6> sarosh;
  Codility::Fish<11> saad;
  Codility::Fish<16> bilal;

  result = sundas.solution(fish_size, fish_direction);
  std::cout << "Sundas result: " << result << std::endl;
  result = sarosh.solution(fish_size, fish_direction);
  std::cout << "Sarosh result: " << result << std::endl;
  result = saad.solution(fish_size, fish_direction);
  std::cout << "Saad result: " << result << std::endl;
  result = bilal.solution(fish_size, fish_direction);
  std::cout << "Bilal result: " << result << std::endl;

  return 0;
}
