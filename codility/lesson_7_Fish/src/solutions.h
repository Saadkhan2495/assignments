#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <vector>
#include <stack>

namespace Codility
{

template <int32_t S = 1>
class Fish
{
public:
  int solution(std::vector<int> &A, std::vector<int> &B)
  {
    std::stack<int> s;

    for (int i = 0; i < (int)A.size(); i++)
    {
      int size = A[i];
      int dir = B[i];
      if (s.empty())
      {
        s.push(i);
      }
      else
      {
        while (!s.empty() && dir - B[s.top()] == -1 && A[s.top()] < size)
        {
          s.pop();
        }
        if (!s.empty())
        {
          if (dir - B[s.top()] != -1)
          {
            s.push(i);
          }
        }
        else
        {
          s.push(i);
        }
      }
    }
    return s.size();
  }
};

template <>
class Fish<6>
{
public:
  int solution(std::vector<int> &A, std::vector<int> &B)
  {

    int alive_fishes = A.size();
    std::stack<int> upstream;

    for (int i = 0; i < int(A.size()); i++)
    {
      if (B[i] == 1)
        upstream.push(A[i]);
      if (B[i] == 0 && !upstream.empty())
      {
        while (!upstream.empty() && A[i] > upstream.top())
        {
          alive_fishes--;
          upstream.pop();
        }
        if (!upstream.empty() && upstream.top() > A[i])
          alive_fishes--;
      }
    }

    return alive_fishes;
  }
};

template <>
class Fish<11>
{
public:
  int solution(std::vector<int> &fish_size, std::vector<int> &fish_direction)
  {
    std::stack<int> fish_s, fish_d;
    for (uint32_t index = 0; index < fish_size.size(); index++)
    {
      if (index == 0)
      {
        fish_s.push(fish_size[index]);
        fish_d.push(fish_direction[index]);
      }
      else
      {
        if (fish_direction[index] == 0 && fish_d.top() != 1)
        {
          fish_s.push(fish_size[index]);
          fish_d.push(fish_direction[index]);
        }
        else if (fish_direction[index] == 0 && fish_d.top() == 1)
        {
          if (fish_s.top() < fish_size[index])
          {
            while (!fish_s.empty() && fish_d.top() == 1 && fish_s.top() < fish_size[index])
            {
              fish_s.pop();
              fish_d.pop();
            }
            if (!fish_s.empty() && fish_s.top() > fish_size[index] && fish_d.top() == 1)
            {
              continue;
            }
            else
            {
              fish_s.push(fish_size[index]);
              fish_d.push(fish_direction[index]);
            }
          }
          else
          {
            continue;
          }
        }
        else if (fish_direction[index] == 1 && fish_d.top() == 1)
        {
          fish_s.push(fish_size[index]);
          fish_d.push(fish_direction[index]);
        }
        else if (fish_direction[index] == 1 && fish_d.top() != 1)
        {
          fish_s.push(fish_size[index]);
          fish_d.push(fish_direction[index]);
        }
      }
    }
    return fish_s.size();
  }
};

template <>
class Fish<16>
{
public:
  int solution(std::vector<int> &A, std::vector<int> &B)
  {
    int N = A.size(), p = 0, totalFishAlive = 0;
    std::stack<int> S;
    for (int i = 0; i < N; i++)
    {
      if (B[i] == 0)
      {
        while (S.size() > 0)
        {
          if (S.top() > A[i])
          {
            break;
          }
          else
            S.pop();
        }
        if (S.size() == 0)
          totalFishAlive++;
      }
      else
      {
        S.push(A[i]);
      }
    }
    return (totalFishAlive + S.size());
  }
};

} // namespace Codility
#endif
