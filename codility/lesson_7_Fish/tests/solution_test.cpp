#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::Fish<1>, Codility::Fish<6>, Codility::Fish<11>, Codility::Fish<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> fish_size = {4, 3, 2, 1, 5};
  std::vector<int> fish_direction = {0, 1, 0, 0, 0};
  int actual = -1;
  int expected = 2;

  // Act
  actual = this->question.solution(fish_size, fish_direction);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_small)
{
  // Arrange
  std::vector<int> fish_size = {4, 2};
  std::vector<int> fish_direction = {1, 0};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(fish_size, fish_direction);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple1)
{
  // Arrange
  std::vector<int> fish_size = {4, 2, 8, 1, 3, 6, 5, 7};
  std::vector<int> fish_direction = {1, 1, 0, 1, 0, 0, 1, 0};
  int actual = -1;
  int expected = 4;

  // Act
  actual = this->question.solution(fish_size, fish_direction);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2)
{
  // Arrange
  std::vector<int> fish_size = {4, 2, 8, 1, 3, 6, 5, 7, 15, 10, 11, 13, 14, 12, 9};
  std::vector<int> fish_direction = {1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0};
  int actual = -1;
  int expected = 9;

  // Act
  actual = this->question.solution(fish_size, fish_direction);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::vector<int> fish_size(1000, 0);
  std::vector<int> fish_direction(1000, 0);
  for (uint32_t index = 0; index < fish_size.size(); index++)
  {
    fish_size[index] = index * 10 + 1;
    if (index % 2 == 0)
    {
      fish_direction[index] = 1;
    }
  }
  int actual = -1;
  int expected = 500;

  // Act
  actual = this->question.solution(fish_size, fish_direction);

  // Assert
  EXPECT_EQ(actual, expected);
}
