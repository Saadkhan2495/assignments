
#include "solutions.h"

#include <iostream>
using namespace std;

void printvec(std::vector<int> const &input)
{
  int size = input.size();
  for (int i = 0; i < size; i++)
  {
    std::cout << input.at(i) << ' ';
  }
}

int main(void)
{
  std::string S = "CAGCCTA";
  std::vector<int> P = {2, 5, 0};
  std::vector<int> Q = {4, 5, 6};
  std::vector<int> impactfactor = {2, 5, 0};
  Codility::GenomicRangeQuery<1> sundas;
  Codility::GenomicRangeQuery<2> sundas1;
  Codility::GenomicRangeQuery<6> sarosh;
  Codility::GenomicRangeQuery<11> saad;
  Codility::GenomicRangeQuery<16> bilal;

  impactfactor = sundas.solution(S, P, Q);
  std::cout << "sundas result: ";
  printvec(impactfactor);
  cout << ::endl;

  impactfactor = sundas.solution(S, P, Q);
  std::cout << "sunda1 result: ";
  printvec(impactfactor);
  cout << ::endl;
  impactfactor = sundas.solution(S, P, Q);
  std::cout << "sarosh result: ";
  printvec(impactfactor);
  cout << ::endl;
  impactfactor = sundas.solution(S, P, Q);
  std::cout << "saad result: ";
  printvec(impactfactor);
  cout << ::endl;
  impactfactor = sundas.solution(S, P, Q);
  std::cout << "bilal result: ";
  printvec(impactfactor);
  cout << ::endl;

  return 0;
}
