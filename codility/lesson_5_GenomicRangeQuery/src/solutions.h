#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <vector>
#include <string>

namespace Codility
{

template <int32_t s1 = 1>
class GenomicRangeQuery
{
public:
    std::vector<int> solution(std::string &S, std::vector<int> &P, std::vector<int> &Q)
    {
        std::vector<int> v(P.size());
        for (int i = 0; i < int(P.size()); i++)
        {
            int min = 100000;
            for (int j = P[i]; j <= Q[i]; j++)
            {
                if (S[j] == 'A')
                {
                    if (min > 1)
                    {
                        min = 1;
                    }
                }
                else if (S[j] == 'C')
                {
                    if (min > 2)
                    {
                        min = 2;
                    }
                }
                else if (S[j] == 'G')
                {
                    if (min > 3)
                    {
                        min = 3;
                    }
                }
                else
                {
                    if (min > 4)
                    {
                        min = 4;
                    }
                }
            }
            v[i] = min;
        }
        return v;
    }
};

template <>
class GenomicRangeQuery<2>
{
public:
    std::vector<int> solution(std::string &S, std::vector<int> &P, std::vector<int> &Q)
    {
        int m = P.size(), n = S.length();
        std::vector<int> impactFactor(m, 4);
        std::vector<std::vector<int>> count(n + 1, std::vector<int>(3));
        for (int i = 0; i < n; ++i)
        {
            count[i + 1][0] = count[i][0] + (S[i] == 'A' ? 1 : 0);
            count[i + 1][1] = count[i][1] + (S[i] == 'C' ? 1 : 0);
            count[i + 1][2] = count[i][2] + (S[i] == 'G' ? 1 : 0);
        }
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < 3; ++j)
                if (count[Q[i] + 1][j] - count[P[i]][j] > 0)
                {
                    impactFactor[i] = j + 1;
                    break;
                }
        }
        return impactFactor;
    }
};

template <>
class GenomicRangeQuery<11>
{
public:
    std::vector<int> solution(std::string DNA_sequence, std::vector<int> p_query, std::vector<int> q_query)
    {

        std::vector<int> answers(p_query.size(), 0);
        std::vector<int> A_occurrence(DNA_sequence.size() + 1, 0);
        std::vector<int> C_occurrence(DNA_sequence.size() + 1, 0);
        std::vector<int> G_occurrence(DNA_sequence.size() + 1, 0);
        std::vector<int> T_occurrence(DNA_sequence.size() + 1, 0);
        for (uint32_t index = 0; index < DNA_sequence.size(); index++)
        {
            if (DNA_sequence[index] == 'A')
            {
                A_occurrence[index + 1] = A_occurrence[index] + 1;
                C_occurrence[index + 1] = C_occurrence[index];
                G_occurrence[index + 1] = G_occurrence[index];
                T_occurrence[index + 1] = T_occurrence[index];
            }
            else if (DNA_sequence[index] == 'C')
            {
                C_occurrence[index + 1] = C_occurrence[index] + 1;
                A_occurrence[index + 1] = A_occurrence[index];
                G_occurrence[index + 1] = G_occurrence[index];
                T_occurrence[index + 1] = T_occurrence[index];
            }
            else if (DNA_sequence[index] == 'G')
            {
                G_occurrence[index + 1] = G_occurrence[index] + 1;
                C_occurrence[index + 1] = C_occurrence[index];
                A_occurrence[index + 1] = A_occurrence[index];
                T_occurrence[index + 1] = T_occurrence[index];
            }
            else
            {
                T_occurrence[index + 1] = T_occurrence[index] + 1;
                C_occurrence[index + 1] = C_occurrence[index];
                G_occurrence[index + 1] = G_occurrence[index];
                A_occurrence[index + 1] = A_occurrence[index];
            }
        }
        for (uint32_t query_no = 0; query_no < p_query.size(); query_no++)
        {
            if (A_occurrence[q_query[query_no] + 1] - A_occurrence[p_query[query_no]] > 0)
            {
                answers[query_no] = 1;
            }
            else if (C_occurrence[q_query[query_no] + 1] - C_occurrence[p_query[query_no]] > 0)
            {
                answers[query_no] = 2;
            }
            else if (G_occurrence[q_query[query_no] + 1] - G_occurrence[p_query[query_no]] > 0)
            {
                answers[query_no] = 3;
            }
            else
            {
                answers[query_no] = 4;
            }
        }
        return answers;
    }
};

template <>
class GenomicRangeQuery<16>
{
public:
    std::vector<int> solution(std::string &S, std::vector<int> &P, std::vector<int> &Q)
    {
        std::vector<int> result(P.size());
        int Slen = S.length();

        int comp[Slen][4] = {0};
        int A_count = 0, C_count = 0, G_count = 0, T_count = 0;

        for (int i = 0; i < Slen; i++)
        {
            if (S[i] == 'A')
            {
                A_count++;
            }
            comp[i][0] = A_count;
            if (S[i] == 'C')
            {
                C_count++;
            }
            comp[i][1] = C_count;
            if (S[i] == 'G')
            {
                G_count++;
            }
            comp[i][2] = G_count;
            if (S[i] == 'T')
            {
                T_count++;
            }
            comp[i][3] = T_count;
        }
        for (int i = 0; i < int(P.size()); i++)
        {
            int A = 0, C = 0, G = 0, T = 0;
            if (S[P[i]] == 'A')
            {
                A = 1;
            }
            else if (S[P[i]] == 'C')
            {
                C = 1;
            }
            else if (S[P[i]] == 'G')
            {
                G = 1;
            }
            else if (S[P[i]] == 'T')
            {
                T = 1;
            }

            if (comp[Q[i]][0] - comp[P[i]][0] + A > 0)
                result[i] = 1;
            else if (comp[Q[i]][1] - comp[P[i]][1] + C > 0)
                result[i] = 2;
            else if (comp[Q[i]][2] - comp[P[i]][2] + G > 0)
                result[i] = 3;
            else if (comp[Q[i]][3] - comp[P[i]][3] + T > 0)
                result[i] = 4;
        }
        return result;
    }
};

template <>
class GenomicRangeQuery<6>
{
public:
    std::vector<int> solution(std::string &S, std::vector<int> &P, std::vector<int> &Q)
    {
        int range = P.size();
        std::vector<int> output = {};
        for (int i = 0; i < range; i++)
        {
            std::string s = S.substr(P[i], Q[i] - P[i] + 1);
            if (s.find('A') != std::string::npos)
            {
                output.push_back(1);
            }
            else if (s.find('C') != std::string::npos)
            {
                output.push_back(2);
            }
            else if (s.find('G') != std::string::npos)
            {
                output.push_back(3);
            }
            else if (s.find('T') != std::string::npos)
            {
                output.push_back(4);
            }
        }

        return output;
    }
};

} // namespace Codility
#endif
