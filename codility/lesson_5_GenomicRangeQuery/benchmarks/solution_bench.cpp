#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>
#include <string>
#include <vector>
template <int32_t s1>
void BM_Solution(benchmark::State &state)
{
  Codility::GenomicRangeQuery<s1> question;

  std::string S = "";
  std::string s2 = "G";
  for (int i = 0; i < 10000; i = i + 1)
  {
    S.append(s2);
  }
  std::vector<int> P;
  for (int i = 0; i < 100; i = i + 1)
  {
    int r = rand() % 50;
    P.push_back(r);
  }
  std::vector<int> Q;
  for (int i = 0; i < 100; i = i + 1)
  {
    int r = 50 + rand() % 50;
    Q.push_back(r);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(S, P, Q));
  }
}
template <int32_t s1>
void BM_Solution_1(benchmark::State &state)
{
  Codility::GenomicRangeQuery<s1> question;

  std::string S = "";
  //std::string s2="ACGT";
  for (int i = 0; i < 10000; i = i + 1)
  {
    int r = rand() % 20;
    if (r >= 1 && r < 5)
    {
      S.append("A");
    }
    if (r >= 5 && r < 10)
    {
      S.append("C");
    }
    if (r >= 5 && r < 15)
    {
      S.append("G");
    }
    if (r >= 15 && r < 20)
    {
      S.append("T");
    }
  }
  std::vector<int> P;
  for (int i = 0; i < 100; i = i + 1)
  {
    int r = rand() % 100;
    P.push_back(r);
  }
  std::vector<int> Q;
  for (int i = 0; i < 100; i = i + 1)
  {
    int r = 1000 + rand() % 50;
    Q.push_back(r);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(S, P, Q));
  }
}
template <int32_t s1>
void BM_Solution_2(benchmark::State &state)
{
  Codility::GenomicRangeQuery<s1> question;

  std::string S = "";
  //std::string s2="ACGT";
  for (int i = 0; i < 1000000; i = i + 1)
  {
    int r = rand() % 20;
    if (r >= 1 && r < 5)
    {
      S.append("A");
    }
    if (r >= 5 && r < 10)
    {
      S.append("C");
    }
    if (r >= 5 && r < 15)
    {
      S.append("G");
    }
    if (r >= 15 && r < 20)
    {
      S.append("T");
    }
  }
  std::vector<int> P;
  for (int i = 0; i < 100; i = i + 1)
  {
    int r = rand() % 50;
    P.push_back(r);
  }
  std::vector<int> Q;
  for (int i = 0; i < 100; i = i + 1)
  {
    int r = 100 + rand() % 50;
    Q.push_back(r);
  }

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(S, P, Q));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 2);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
BENCHMARK_TEMPLATE(BM_Solution_1, 1);
BENCHMARK_TEMPLATE(BM_Solution_1, 2);
BENCHMARK_TEMPLATE(BM_Solution_1, 6);
BENCHMARK_TEMPLATE(BM_Solution_1, 11);
BENCHMARK_TEMPLATE(BM_Solution_1, 16);
BENCHMARK_TEMPLATE(BM_Solution_2, 1);
BENCHMARK_TEMPLATE(BM_Solution_2, 2);
BENCHMARK_TEMPLATE(BM_Solution_2, 6);
BENCHMARK_TEMPLATE(BM_Solution_2, 11);
BENCHMARK_TEMPLATE(BM_Solution_2, 16);
