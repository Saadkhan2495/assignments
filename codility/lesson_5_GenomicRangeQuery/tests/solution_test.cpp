#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
#include <string>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::GenomicRangeQuery<1>, Codility::GenomicRangeQuery<2>, Codility::GenomicRangeQuery<6>, Codility::GenomicRangeQuery<11>, Codility::GenomicRangeQuery<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::string S = "CAGCCTA";
  std::vector<int> P = {2, 5, 0};
  std::vector<int> Q = {4, 5, 6};
  std::vector<int> actual = {};
  std::vector<int> expected = {2, 4, 1};

  // Act
  actual = this->question.solution(S, P, Q);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_single)
{
  // Arrange
  std::string S = "C";
  std::vector<int> P = {0};
  std::vector<int> Q = {0};
  std::vector<int> actual = {};
  std::vector<int> expected = {2};

  // Act
  actual = this->question.solution(S, P, Q);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, extreme_double)
{
  // Arrange
  std::string S = "CA";
  std::vector<int> P = {0};
  std::vector<int> Q = {1};
  std::vector<int> actual = {};
  std::vector<int> expected = {1};

  // Act
  actual = this->question.solution(S, P, Q);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, simple)
{
  // Arrange
  std::string S = "CAGGTTCATCTACGTAC";
  std::vector<int> P = {0, 5, 6};
  std::vector<int> Q = {3, 8, 10};
  std::vector<int> actual = {};
  std::vector<int> expected = {1, 1, 1};

  // Act
  actual = this->question.solution(S, P, Q);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, small_length_string)
{
  // Arrange
  std::string S = "CATCGATTCATTCA";
  std::vector<int> P = {2, 3, 7};
  std::vector<int> Q = {4, 8, 10};
  std::vector<int> actual = {};
  std::vector<int> expected = {2, 1, 1};

  // Act
  actual = this->question.solution(S, P, Q);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, small_random)
{
  // Arrange
  std::string S = "CAGGTTCATCTACGTACCTACGTACTAGTATCATGGCGATGCATGCATAGCTGCATGCACTCGATCGTCAGTACCG";
  std::vector<int> P = {10, 18, 25};
  std::vector<int> Q = {11, 25, 30};
  std::vector<int> actual = {};
  std::vector<int> expected = {1, 1, 1};

  // Act
  actual = this->question.solution(S, P, Q);

  // Assert
  EXPECT_EQ(actual, expected);
}
