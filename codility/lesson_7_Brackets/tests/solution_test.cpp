#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <string>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::Brackets<1>, Codility::Brackets<6>, Codility::Brackets<11>, Codility::Brackets<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  std::string input = "{[()()]}";
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, example2)
{
  // Arrange
  std::string input = "([)()]";
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, negative_match)
{
  // Arrange
  std::string input = "()[]{}(()){{[]}}{]{}}";
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, empty)
{
  // Arrange
  std::string input = "";
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, simple_grouped)
{
  // Arrange
  std::string input = "{{{{{[[[[]]]]}}((((((((()))))))))}}}";
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
