#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
#include <stack>
#include <string>
// Bilal: I have updated my code.....

namespace Codility
{

template <int32_t S = 1>
class Brackets
{
public:
    int solution(std::string &str)
    {
        if (str.length() == 0)
        {
            return 1;
        }
        std::stack<char> brackets;
        for (int i = 0; i < (int)str.length(); i++)
        {
            if (str[i] == '(' || str[i] == '{' || str[i] == '[')
            {
                brackets.push(str[i]);
            }
            else
            {
                if (brackets.size() == 0)
                {
                    return 0;
                }
                if (str[i] == ')')
                {
                    if (brackets.top() == '(')
                    {
                        brackets.pop();
                    }
                    else
                    {
                        return 0;
                    }
                }
                if (str[i] == '}')
                {
                    if (brackets.top() == '{')
                    {
                        brackets.pop();
                    }
                    else
                    {
                        return 0;
                    }
                }
                if (str[i] == ']')
                {
                    if (brackets.top() == '[')
                    {
                        brackets.pop();
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        if (brackets.empty())
        {
            return 1;
        }
        return 0;
    }
};

template <>
class Brackets<16>
{
public:
    int solution(std::string &S)
    {
        int N = S.size();
        std::stack<char> checkStack;
        for (int i = 0; i < N; i++)
        {
            if (S[i] == '(' || S[i] == '[' || S[i] == '{')
            {
                checkStack.push(S[i]);
            }
            else
            {
                if (checkStack.empty())
                    return 0;
                if ('}' == S[i] && '{' != checkStack.top())
                    return 0;
                if (']' == S[i] && '[' != checkStack.top())
                    return 0;
                if (')' == S[i] && '(' != checkStack.top())
                    return 0;
                checkStack.pop();
            }
        }
        if (checkStack.empty())
            return 1;
        return 0;
    }
};

template <>
class Brackets<11>
{
public:
    int solution(std::string &input_string)
    {
        std::stack<char> brackets;
        for (uint32_t index = 0; index < input_string.size(); index++)
        {
            if (input_string[index] == '(' || input_string[index] == '{' || input_string[index] == '[')
            {
                brackets.push(input_string[index]);
            }
            else if (input_string[index] == ')')
            {
                if (brackets.empty() || brackets.top() != '(')
                {
                    return 0;
                }
                else
                {
                    brackets.pop();
                }
            }
            else if (input_string[index] == '}')
            {
                if (brackets.empty() || brackets.top() != '{')
                {
                    return 0;
                }
                else
                {
                    brackets.pop();
                }
            }
            else if (input_string[index] == ']')
            {
                if (brackets.empty() || brackets.top() != '[')
                {
                    return 0;
                }
                else
                {
                    brackets.pop();
                }
            }
        }
        if (brackets.empty())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
};

template <>
class Brackets<6>
{
public:
    int solution(std::string &S)
    {
        std::stack<char> brackets;
        for (long int i = 0; i < (int)S.length(); i++)
        {
            if (S[i] == '(')
            {
                brackets.push(')');
            }
            else if (S[i] == '[')
            {
                brackets.push(']');
            }
            else if (S[i] == '{')
            {
                brackets.push('}');
            }
            else if (S[i] == ')' || S[i] == ']' || S[i] == '}')
            {
                if (brackets.empty() || brackets.top() != S[i])
                    return 0;

                brackets.pop();
            }
        }

        if (brackets.empty())
            return 1;
        else
            return 0;
    }
};

} // namespace Codility
#endif
