
#include "solutions.h"
#include <vector>
#include <iostream>
#include <string>
int main(void)
{
  std::string input = "{[()()]}";
  int result = -1;

  Codility::Brackets<1> sundas;
  Codility::Brackets<6> sarosh;
  Codility::Brackets<11> saad;
  Codility::Brackets<16> bilal;
  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;
  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;
  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;
  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
