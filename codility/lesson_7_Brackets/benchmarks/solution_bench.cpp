#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>
#include <string>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::Brackets<S> question;
  std::string input = "";
  for (int i = 0; i < 200000; i++)
  {
    if (i < 100000)
    {
      input = input + "(";
    }
    else
    {
      input = input + ")";
    }
  }
  input = input + ")(";
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
template <int32_t S>
void BM_Solution_1(benchmark::State &state)
{
  Codility::Brackets<S> question;
  std::string input = "";
  for (int i = 0; i < 200000; i++)
  {
    if (i < 100001)
    {
      input = input + "(";
    }
    else
    {
      input = input + ")";
    }
  }
  input = input + ")(()";
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
template <int32_t S>
void BM_Solution_2(benchmark::State &state)
{
  Codility::Brackets<S> question;
  std::string T = "{{{{{{{{{{{}}}}}}}}}}}";
  std::string input = "";
  while (input.length() < 17700)
  {

    input = input + "(" + T + T + T + ")";
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
template <int32_t S>
void BM_Solution_3(benchmark::State &state)
{
  Codility::Brackets<S> question;
  std::string T = "{}";
  std::string input = "";

  while (input.length() < 49000)
  {

    input = input + "(" + T + T + ")";
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
template <int32_t S>
void BM_Solution_4(benchmark::State &state)
{
  Codility::Brackets<S> question;
  std::string T = "{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}";
  std::string input = "";

  while (input.length() < 120000)
  {

    input = input + "[" + T + T + "]";
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}
BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
BENCHMARK_TEMPLATE(BM_Solution_1, 1);
BENCHMARK_TEMPLATE(BM_Solution_1, 6);
BENCHMARK_TEMPLATE(BM_Solution_1, 11);
BENCHMARK_TEMPLATE(BM_Solution_1, 16);
BENCHMARK_TEMPLATE(BM_Solution_2, 1);
BENCHMARK_TEMPLATE(BM_Solution_2, 6);
BENCHMARK_TEMPLATE(BM_Solution_2, 11);
BENCHMARK_TEMPLATE(BM_Solution_2, 16);
BENCHMARK_TEMPLATE(BM_Solution_3, 1);
BENCHMARK_TEMPLATE(BM_Solution_3, 6);
BENCHMARK_TEMPLATE(BM_Solution_3, 11);
BENCHMARK_TEMPLATE(BM_Solution_3, 16);
BENCHMARK_TEMPLATE(BM_Solution_4, 1);
BENCHMARK_TEMPLATE(BM_Solution_4, 6);
BENCHMARK_TEMPLATE(BM_Solution_4, 11);
BENCHMARK_TEMPLATE(BM_Solution_4, 16);
