#include "solutions.h"
#include <benchmark/benchmark.h>
#include <limits>
#include <vector>

template <int32_t S>
void medium1(benchmark::State &state)
{
  Codility::PermMissingElem<S> question;

  std::vector<int> inputvector = {};
  for (int i = 2; i < 10002; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium1, 1);
BENCHMARK_TEMPLATE(medium1, 2);
BENCHMARK_TEMPLATE(medium1, 6);
BENCHMARK_TEMPLATE(medium1, 11);
BENCHMARK_TEMPLATE(medium1, 16);

template <int32_t S>
void medium2(benchmark::State &state)
{
  Codility::PermMissingElem<S> question;

  std::vector<int> inputvector = {};
  for (int i = 2; i < 10002; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(medium2, 1);
BENCHMARK_TEMPLATE(medium2, 2);
BENCHMARK_TEMPLATE(medium2, 6);
BENCHMARK_TEMPLATE(medium2, 11);
BENCHMARK_TEMPLATE(medium2, 16);

template <int32_t S>
void large_range(benchmark::State &state)
{
  Codility::PermMissingElem<S> question;

  std::vector<int> inputvector = {};
  for (int i = 2; i < 99000; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large_range, 1);
BENCHMARK_TEMPLATE(large_range, 2);
BENCHMARK_TEMPLATE(large_range, 6);
BENCHMARK_TEMPLATE(large_range, 11);
BENCHMARK_TEMPLATE(large_range, 16);

template <int32_t S>
void large1(benchmark::State &state)
{
  Codility::PermMissingElem<S> question;

  std::vector<int> inputvector = {};
  for (int i = 1; i < 99000; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large1, 1);
BENCHMARK_TEMPLATE(large1, 2);
BENCHMARK_TEMPLATE(large1, 6);
BENCHMARK_TEMPLATE(large1, 11);
BENCHMARK_TEMPLATE(large1, 16);

template <int32_t S>
void large2(benchmark::State &state)
{
  Codility::PermMissingElem<S> question;

  std::vector<int> inputvector = {};
  for (int i = 1; i < 99990; i++)
    inputvector.push_back(i);

  while (state.KeepRunning())
  {
    question.solution(inputvector);
  }
}

BENCHMARK_TEMPLATE(large2, 1);
BENCHMARK_TEMPLATE(large2, 2);
BENCHMARK_TEMPLATE(large2, 6);
BENCHMARK_TEMPLATE(large2, 11);
BENCHMARK_TEMPLATE(large2, 16);