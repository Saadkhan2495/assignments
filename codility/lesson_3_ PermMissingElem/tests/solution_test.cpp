#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::PermMissingElem<1>, Codility::PermMissingElem<2>, Codility::PermMissingElem<6>, Codility::PermMissingElem<11>, Codility::PermMissingElem<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> inputArray = {2, 3, 1, 5};
  int expected = 4;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, empty_and_single)
{
  // Arrange
  std::vector<int> inputArray = {};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);

  // Arrange
  inputArray = {1};
  expected = 2;
  actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, missing_first_or_last)
{
  // Arrange
  std::vector<int> inputArray = {2, 3, 4, 5, 6, 7, 8};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);

  // Arrange
  inputArray = {1, 2, 3, 4, 5, 6, 7};
  expected = 8;
  actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, single)
{
  // Arrange
  std::vector<int> inputArray = {2};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, Double)
{
  // Arrange
  std::vector<int> inputArray = {2, 3};
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple)
{
  // Arrange
  std::vector<int> inputArray = {1, 2, 3, 5, 6, 7, 8};
  int expected = 4;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

//Performance tests

TYPED_TEST(SolutionTest, medium1)
{
  // Arrange
  std::vector<int> inputArray{};
  for (int i = 2; i < 10002; i++)
    inputArray.push_back(i);
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium2)
{
  // Arrange
  std::vector<int> inputArray{};
  for (int i = 2; i < 10006; i++)
    inputArray.push_back(i);
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large_range)
{
  // Arrange
  std::vector<int> inputArray{};
  for (int i = 2; i < 99000; i++)
    inputArray.push_back(i);
  int expected = 1;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large1)
{
  // Arrange
  std::vector<int> inputArray{};
  for (int i = 1; i < 99000; i++)
    inputArray.push_back(i);
  int expected = 99000;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, large2)
{
  // Arrange
  std::vector<int> inputArray{};
  for (int i = 1; i < 99990; i++)
    inputArray.push_back(i);
  int expected = 99990;
  int actual = -1;
  // Act
  actual = this->question.solution(inputArray);

  // Assert
  EXPECT_EQ(actual, expected);
}