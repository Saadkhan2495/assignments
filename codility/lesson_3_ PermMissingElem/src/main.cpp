
#include "solutions.h"
#include <vector>
#include <iostream>
using namespace std;
int main(void)
{
  int result = -1;
  vector<int> input = {2, 3, 1, 5};
  Codility::PermMissingElem<1> sundas1;
  Codility::PermMissingElem<2> sundas2;
  Codility::PermMissingElem<6> sarosh;
  Codility::PermMissingElem<11> saad;
  Codility::PermMissingElem<16> bilal;
  result = sundas1.solution(input);
  std::cout << "sundas1 result: " << result << std::endl;

  result = sundas2.solution(input);
  std::cout << "sundas1 result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;

  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;

  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}