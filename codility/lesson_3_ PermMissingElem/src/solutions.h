#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <algorithm>
#include <numeric>

#include <vector>

namespace Codility
{

template <int32_t S = 1>
class PermMissingElem
{
public:
    int solution(std::vector<int> &A)
    {

        sort(A.begin(), A.end());
        int N = A.size();

        for (int i = 0; i < N; i++)
        {
            if (A[i] != (i + 1))
            {
                return i + 1;
            }
        }

        return N + 1;
    }
};

template <>
class PermMissingElem<2>
{
public:
    int solution(std::vector<int> &A)
    {
        int N = A.size();
        long sum_expected = ((N + 1) * (N + 2)) / 2;
        long sum_actual = accumulate(A.begin(), A.end(), 0);
        return sum_expected - sum_actual;
    }
};

template <>
class PermMissingElem<16>
{
public:
    int solution(std::vector<int> &A)
    {
        sort(A.begin(), A.end());
        for (int i = 0; i < int(A.size()); i++)
        {
            if (A[i] != i + 1)
            {
                return i + 1;
            }
        }
        return A.size() + 1;
    }
};

template <>
class PermMissingElem<6>
{
public:
    int solution(std::vector<int> &A)
    {
        long long int vector_sum = accumulate(A.begin(), A.end(), 0);
        long int size_of_vector = A.size() + 1;
        long long int sum_all_numbers_till_size = size_of_vector * (size_of_vector + 1) / 2;
        return sum_all_numbers_till_size - vector_sum;
    }
};

template <>
class PermMissingElem<11>
{
public:
    int solution(std::vector<int> &input_array)
    {
        uint32_t array_size = input_array.size();
        std::vector<int> sorted_input_array;
        sorted_input_array = input_array;
        std::sort(sorted_input_array.begin(), sorted_input_array.end());
        if (array_size == 0)
        {
            return 1;
        }
        if (sorted_input_array[0] != 1)
        {
            return 1;
        }
        else
        {
            for (uint32_t index = 0; index <= (array_size - 1); index++)
            {
                if (sorted_input_array[index] != sorted_input_array[index + 1] - 1)
                {
                    return (sorted_input_array[index] + 1);
                }
            }
        }
        return 0;
    }
};

} // namespace Codility
#endif
