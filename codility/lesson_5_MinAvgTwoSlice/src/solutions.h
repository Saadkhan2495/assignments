#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <vector>

namespace Codility
{

template <int32_t S = 1>
class MinAvgTwoSlice
{
public:
  int solution(std::vector<int> &A)
  {

    // Find prefix sum.
    int N = A.size();
    std::vector<int> ps(N + 1, 0);

    for (int i = 1; i <= N; i++)
    {
      ps[i] = A[i - 1] + ps[i - 1];
    }

    int lft_idx, min_lft_idx;
    double avg_here, min_avg, avg_of_two, avg_with_prev;

    // Initialize variables at the first possible slice (A[0:1]).
    lft_idx = min_lft_idx = 0;
    avg_here = min_avg = (A[0] + A[1]) / 2.0;

    // Find min average of every slice that ends at ith element,
    // starting at i = 2.
    for (int i = 2; i < N; i++)
    {

      // average of A[lft_idx : i]
      avg_with_prev = ((double)ps[i + 1] - ps[lft_idx]) /
                      (i - lft_idx + 1);

      // average of A[i - 1 : i]
      avg_of_two = (A[i - 1] + A[i]) / 2.0;

      // Find minimum and update lft_idx of slice
      // (previous lft_idx or i - 1).
      if (avg_of_two < avg_with_prev)
      {
        avg_here = avg_of_two;
        lft_idx = i - 1;
      }
      else
        avg_here = avg_with_prev;

      // Keep track of minimum so far and its left index.
      if (avg_here < min_avg)
      {
        min_avg = avg_here;
        min_lft_idx = lft_idx;
      }
    }

    return min_lft_idx;
  }
};

template <>
class MinAvgTwoSlice<11>
{
public:
  int solution(std::vector<int> input_array)
  {
    int array_size = input_array.size();
    int min_index = 0;
    float min_average = 10000;
    float average_2;
    float average_3;
    for (int index = 0; index < array_size - 1; index++)
    {
      average_2 = (input_array[index] + input_array[index + 1]) / 2.0;
      if (average_2 < min_average)
      {
        min_average = average_2;
        min_index = index;
      }
      if (index <= (array_size - 3))
      {
        average_3 = (input_array[index] + input_array[index + 1] + input_array[index + 2]) / 3.0;
        if (average_3 < min_average)
        {
          min_average = average_3;
          min_index = index;
        }
      }
    }
    return min_index;
  }
};

template <>
class MinAvgTwoSlice<16>
{
public:
  int solution(std::vector<int> &A)
  {
    int N = A.size();
    std::vector<int> incSum(N);
    int s = 0;

    for (int i = 0; i < N; i++)
    {
      s += A[i];
      incSum[i] = s;
    }

    int start = 0;
    int min_start = start;
    double min_avg = double(incSum[1] - incSum[start] + A[start]) / (1 - start + 1);

    for (int i = 1; i < N; i++)
    {
      double avg = double(incSum[i] - incSum[start] + A[start]) / (i - start + 1);

      if (avg < min_avg)
      {
        min_avg = avg;
        min_start = start;
      }

      if (A[i] < min_avg)
      {
        start = i;
      }
    }

    return min_start;
  }
};

template <>
class MinAvgTwoSlice<6>
{
public:
  int solution(std::vector<int> &A)
  {
    double minAvg = 100000;
    int index = 0;

    if (int(A.size()) <= 2)
    {

      return 0;
    }
    int len = A.size() - 2;
    for (int i = 0; i < len; i++)
    {

      if ((A[i] + A[i + 1]) / 2.0 < minAvg)
      {
        minAvg = (A[i] + A[i + 1]) / 2.0;
        index = i;
      }

      if ((A[i] + A[i + 1] + A[i + 2]) / 3.0 < minAvg)
      {

        minAvg = (A[i] + A[i + 1] + A[i + 2]) / 3.0;
        index = i;
      }
    }

    if ((A[len] + A[len + 1]) / 2.0 < minAvg)
    {

      minAvg = (A[len] + A[len + 1]) / 2.0;
      index = len;
    }

    return index;
  }
};

} // namespace Codility
#endif
