
#include "solutions.h"

#include <iostream>

int main(void)
{
  std::vector<int> input = {4, 2, 2, 5, 1, 5, 8};
  int result = -1;

  Codility::MinAvgTwoSlice<1> sundas;
  Codility::MinAvgTwoSlice<6> sarosh;
  Codility::MinAvgTwoSlice<11> saad;
  Codility::MinAvgTwoSlice<16> bilal;
  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;
  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;
  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;
  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
