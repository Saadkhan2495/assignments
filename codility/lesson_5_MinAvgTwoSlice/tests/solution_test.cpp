#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>
#include <vector>
#include <cstdlib>
template <typename TypeName> class SolutionTest : public testing::Test {
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::MinAvgTwoSlice<1>, Codility::MinAvgTwoSlice<6> ,Codility::MinAvgTwoSlice<16>,Codility::MinAvgTwoSlice<11>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example) {
  // Arrange
std::vector<int> input={4,2,2,5,1,5,8};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, double_quadraple_1) {
  // Arrange
std::vector<int> input={3,2};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, double_quadraple_2) {
  // Arrange
std::vector<int> input={2,3,1,5};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple1) {
  // Arrange
std::vector<int> input={1,5,1,4,1,3,2,8,11};
  int actual = -1;
  int expected = 2;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2) {
  // Arrange
std::vector<int> input={2,1,6,8,9,1,4,8,9,0,1,9,0};
  int actual = -1;
  int expected = 9;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, small_random) {
  // Arrange
  /*srand(2);
std::vector<int> input(100);
std::generate(input.begin(), input.end(), std::rand);*/
std::vector<int> input={1,3,4,5,7,8,9,3,5,4,5,6,3,4,6,3,5,7,8,9,5,3,5,5,54,3,3,12,0,7,8};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
TYPED_TEST(SolutionTest, small_random_2) {
  // Arrange
  /*srand(2);
std::vector<int> input(1000);
std::generate(input.begin(), input.end(), std::rand);*/
std::vector<int> input={1,3,4,5,4,7,1,7,8,9,3,5,4,5,6,7,9,0,2,3,3,4,6,5,3,1,3,5,7,8,9,5,3,5,5,54,3,3,12,0,7,8};
  int actual = -1;
  int expected = 17;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

