#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_medium1(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array = {9, 8, 5, 6, 5, 7, 8, 6, 2, 5, 9, 9};
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_medium2(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array = {9, 8, 5, 6, 5, 7, 8, 6, 2, 5, 9, 9, 10, 11, 2, 3, 5, 2, 1, 5, 3, 6, 2, 5};
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_medium3(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array = {9, 8, 5, 6, 5, 8, 4, 1, 2, 7, 6, 3, 2, 7, 2, 1, 5, 7, 5, 7, 8, 6, 2, 5, 9, 9};
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_medium4(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array = {5, 2, 3, 3, 7, 2, 1, 5, 6, 7, 2, 4, 2, 4, 3, 1, 6, 8, 5, 2, 4, 9, 8, 5, 6, 5, 7, 8, 6, 2, 5, 9, 9};
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_pyramid(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array(100000, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    if (index < 50000)
    {
      input_array[index] = index + 1;
    }
    else
    {
      input_array[index] = 100000 - index;
    }
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_increasing(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array(100000, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index + 1;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_decreasing(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array(100000, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = 100000 - index;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_up_to_20(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array(20, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index + 1;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_up_to_100(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array(20, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index + 1;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

template <int32_t S>
void BM_large_max(benchmark::State &state)
{
  Codility::StoneWall<S> question;
  std::vector<int> input_array(100000, 0);
  for (uint32_t index = 0; index < input_array.size(); index++)
  {
    input_array[index] = index * 1000 + 1;
  }
  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input_array));
  }
}

BENCHMARK_TEMPLATE(BM_medium1, 1);
BENCHMARK_TEMPLATE(BM_medium1, 6);
BENCHMARK_TEMPLATE(BM_medium1, 11);
BENCHMARK_TEMPLATE(BM_medium1, 16);

BENCHMARK_TEMPLATE(BM_medium2, 1);
BENCHMARK_TEMPLATE(BM_medium2, 6);
BENCHMARK_TEMPLATE(BM_medium2, 11);
BENCHMARK_TEMPLATE(BM_medium2, 16);

BENCHMARK_TEMPLATE(BM_medium3, 1);
BENCHMARK_TEMPLATE(BM_medium3, 6);
BENCHMARK_TEMPLATE(BM_medium3, 11);
BENCHMARK_TEMPLATE(BM_medium3, 16);

BENCHMARK_TEMPLATE(BM_medium4, 1);
BENCHMARK_TEMPLATE(BM_medium4, 6);
BENCHMARK_TEMPLATE(BM_medium4, 11);
BENCHMARK_TEMPLATE(BM_medium4, 16);

BENCHMARK_TEMPLATE(BM_large_pyramid, 1);
BENCHMARK_TEMPLATE(BM_large_pyramid, 6);
BENCHMARK_TEMPLATE(BM_large_pyramid, 11);
BENCHMARK_TEMPLATE(BM_large_pyramid, 16);

BENCHMARK_TEMPLATE(BM_large_increasing, 1);
BENCHMARK_TEMPLATE(BM_large_increasing, 6);
BENCHMARK_TEMPLATE(BM_large_increasing, 11);
BENCHMARK_TEMPLATE(BM_large_increasing, 16);

BENCHMARK_TEMPLATE(BM_large_decreasing, 1);
BENCHMARK_TEMPLATE(BM_large_decreasing, 6);
BENCHMARK_TEMPLATE(BM_large_decreasing, 11);
BENCHMARK_TEMPLATE(BM_large_decreasing, 16);

BENCHMARK_TEMPLATE(BM_large_up_to_20, 1);
BENCHMARK_TEMPLATE(BM_large_up_to_20, 6);
BENCHMARK_TEMPLATE(BM_large_up_to_20, 11);
BENCHMARK_TEMPLATE(BM_large_up_to_20, 16);

BENCHMARK_TEMPLATE(BM_large_up_to_100, 1);
BENCHMARK_TEMPLATE(BM_large_up_to_100, 6);
BENCHMARK_TEMPLATE(BM_large_up_to_100, 11);
BENCHMARK_TEMPLATE(BM_large_up_to_100, 16);

BENCHMARK_TEMPLATE(BM_large_max, 1);
BENCHMARK_TEMPLATE(BM_large_max, 6);
BENCHMARK_TEMPLATE(BM_large_max, 11);
BENCHMARK_TEMPLATE(BM_large_max, 16);