#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::StoneWall<1>, Codility::StoneWall<6>, Codility::StoneWall<11>, Codility::StoneWall<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> input_array = {8, 8, 5, 7, 9, 8, 7, 4, 8};
  int actual = -1;
  int expected = 7;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple1)
{
  // Arrange
  std::vector<int> input_array = {5, 3, 4, 8, 9, 2, 4, 8, 8};
  int actual = -1;
  int expected = 8;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2)
{
  // Arrange
  std::vector<int> input_array = {1, 2, 3, 5, 8, 6, 7, 4, 2, 4};
  int actual = -1;
  int expected = 9;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple3)
{
  // Arrange
  std::vector<int> input_array = {4, 5, 5, 6, 8, 5, 7, 2};
  int actual = -1;
  int expected = 6;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple4)
{
  // Arrange
  std::vector<int> input_array = {5, 5, 6, 8, 8, 7, 4, 2, 5, 8, 9, 1};
  int actual = -1;
  int expected = 10;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, boundary_case_1)
{
  // Arrange
  std::vector<int> input_array = {1, 2, 3, 4, 5};
  int actual = -1;
  int expected = 5;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, boundary_case_2)
{
  // Arrange
  std::vector<int> input_array = {8, 8, 8, 8, 8};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input_array);

  // Assert
  EXPECT_EQ(actual, expected);
}