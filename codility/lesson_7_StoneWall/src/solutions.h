#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <vector>
#include <stack>

namespace Codility
{

template <int32_t S = 1>
class StoneWall
{
public:
#include <stack>
    int solution(std::vector<int> &H)
    {
        std::stack<int> s;
        int count = 1;

        for (int i = 0; i < (int)H.size() - 1; i++)
        {
            if (H[i + 1] > H[i])
            {
                s.push(H[i]);
                count++;
            }
            else if (H[i + 1] < H[i])
            {
                if (s.empty())
                {
                    s.push(H[i]);
                    count++;
                }
                else if (H[i + 1] > s.top())
                {
                    count++;
                }
                else if (H[i + 1] < s.top())
                {
                    while (!s.empty() && H[i + 1] < s.top())
                    {
                        s.pop();
                    }
                    if (s.empty())
                    {
                        count++;
                    }
                    else if (H[i + 1] > s.top())
                    {
                        count++;
                    }
                }
                else
                {
                    s.pop();
                }
            }
        }
        return count;
    }
};

template <>
class StoneWall<6>
{
public:
    int solution(std::vector<int> &H)
    {

        std::stack<int> previous;

        int count = 0;
        int size = H.size();
        for (int i = 0; i < size; i++)
        {
            while (!previous.empty() && H[i] < previous.top())
            {
                previous.pop();
            }

            if (previous.empty() || H[i] > previous.top())
            {
                previous.push(H[i]);
                count++;
            }
        }
        return count;
    }
};

template <>
class StoneWall<11>
{
public:
    int solution(std::vector<int> &input_array)
    {
        std::stack<int> block;
        int no_of_blocks = 0;
        for (uint32_t index = 0; index < input_array.size(); index++)
        {
            if (index == 0)
            {
                block.push(input_array[index]);
                no_of_blocks++;
            }
            else
            {
                if (!block.empty() && input_array[index] < block.top())
                {
                    while (!block.empty() && input_array[index] < block.top())
                    {
                        block.pop();
                    }
                }
                if (!block.empty() && input_array[index] == block.top())
                {
                    continue;
                }
                else
                {
                    block.push(input_array[index]);
                    no_of_blocks++;
                }
            }
        }
        return no_of_blocks;
    }
};

template <>
class StoneWall<16>
{
public:
    int solution(std::vector<int> &H)
    {
        int N = H.size(), countBlock = 0;
        std::stack<int> checkStack;
        for (int i = 0; i < N; i++)
        {

            if ((checkStack.size() > 0))
                while ((checkStack.size() > 0) && (checkStack.top() > H[i]))
                {
                    checkStack.pop();
                }
            if ((checkStack.size() > 0) && (checkStack.top() == H[i]))
            {
                continue;
            }
            else
            {
                countBlock++;
                checkStack.push(H[i]);
            }
        }
        return countBlock;
    }
};

} // namespace Codility
#endif
