
#include "solutions.h"
#include <vector>
#include <iostream>
int main(void)
{
  std::vector<int> input = {8, 8, 5, 7, 9, 8, 7, 4, 8};
  int result = -1;

  Codility::StoneWall<1> sundas;
  Codility::StoneWall<6> sarosh;
  Codility::StoneWall<11> saad;
  Codility::StoneWall<16> bilal;

  result = sundas.solution(input);
  std::cout << "Sundas result: " << result << std::endl;
  result = sarosh.solution(input);
  std::cout << "Sarosh result: " << result << std::endl;
  result = saad.solution(input);
  std::cout << "Saad result: " << result << std::endl;
  result = bilal.solution(input);
  std::cout << "Bilal result: " << result << std::endl;

  return 0;
}
