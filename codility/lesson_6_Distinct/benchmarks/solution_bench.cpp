#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::Distinct<S> question;
  std::vector<int> input = {};
  srand(6);
  for (int i = 0; i < 10000; i++)
<<<<<<< HEAD
    input.push_back(rand() % 100001);
=======
    input.push_back(rand() % 100000);
>>>>>>> 9e712d134313a8a81dc5a84837e215ff1037cd61

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

template <int32_t S>
void BM_Solution_1(benchmark::State &state)
{
  Codility::Distinct<S> question;
  std::vector<int> input = {};
  srand(6);
  for (int i = 0; i < 100000; i++)
    input.push_back(-1000000 + rand() % 2000000);

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

template <int32_t S>
void BM_Solution_2(benchmark::State &state)
{
  Codility::Distinct<S> question;
  std::vector<int> input = {};
  srand(9);
  for (int i = 0; i < 100000; i++)
    input.push_back(-1000000 + rand() % 2000000);

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(input));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1);
BENCHMARK_TEMPLATE(BM_Solution, 6);
BENCHMARK_TEMPLATE(BM_Solution, 11);
BENCHMARK_TEMPLATE(BM_Solution, 16);
BENCHMARK_TEMPLATE(BM_Solution_1, 1);
BENCHMARK_TEMPLATE(BM_Solution_1, 6);
BENCHMARK_TEMPLATE(BM_Solution_1, 11);
BENCHMARK_TEMPLATE(BM_Solution_1, 16);
BENCHMARK_TEMPLATE(BM_Solution_2, 1);
BENCHMARK_TEMPLATE(BM_Solution_2, 6);
BENCHMARK_TEMPLATE(BM_Solution_2, 11);
BENCHMARK_TEMPLATE(BM_Solution_2, 16);