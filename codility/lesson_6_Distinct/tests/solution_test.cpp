#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::Distinct<1>, Codility::Distinct<6>, Codility::Distinct<11>, Codility::Distinct<16>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example)
{
  // Arrange
  std::vector<int> input = {2, 1, 1, 2, 3, 1};
  int actual = -1;
  int expected = 3;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_empty)
{
  // Arrange
  std::vector<int> input = {};
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_single)
{
  // Arrange
  std::vector<int> input = {1};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, one_value)
{
  // Arrange
  std::vector<int> input = {1, 1, 1, 1, 1, 1, 1, 1, 1};
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_two_value)
{
  // Arrange
  std::vector<int> input = {1, 0};
  int actual = -1;
  int expected = 2;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_negative)
{
  // Arrange
  std::vector<int> input = {-1000000, -1, -1000000, -265, -1};
  int actual = -1;
  int expected = 3;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extreme_big_values)
{
  // Arrange
  std::vector<int> input = {1000000, 1000000, -1000000, 265000, 999999};
  int actual = -1;
  int expected = 4;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium1)
{
  // Arrange
  std::vector<int> input = {};
  srand(5);
  for (int i = 0; i < 100; i++)
    input.push_back(rand() % 1000);

  int actual = -1;
  int expected = 94;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium2)
{
  // Arrange
  std::vector<int> input = {};
  srand(6);
  for (int i = 0; i < 200; i++)
    input.push_back(rand() % 1000);

  int actual = -1;
  int expected = 180;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, medium3)
{
  // Arrange
  std::vector<int> input = {};
  srand(6);
  for (int i = 0; i < 200; i++)
    input.push_back(rand() % 10);

  int actual = -1;
  int expected = 10;

  // Act
  actual = this->question.solution(input);

  // Assert
  EXPECT_EQ(actual, expected);
}
