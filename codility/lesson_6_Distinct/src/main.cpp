
#include "solutions.h"
#include <vector>
#include <iostream>
int main(void)
{
  std::vector<int> input = {2, 1, 1, 2, 3, 1};
  int result = -1;

  Codility::Distinct<1> sundas;
  Codility::Distinct<6> sarosh;
  Codility::Distinct<11> saad;
  Codility::Distinct<16> bilal;
  result = sundas.solution(input);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(input);
  std::cout << "sarosh result: " << result << std::endl;
  result = saad.solution(input);
  std::cout << "saad result: " << result << std::endl;
  result = bilal.solution(input);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
