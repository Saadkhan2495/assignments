#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <numeric>
#include <vector>
#include <bits/stdc++.h>

namespace Codility
{

template <int32_t S = 1>
class Distinct
{
public:
  int solution(std::vector<int> &A)
  {
    int N = A.size();
    if (N == 0)
    {
      return 0;
    }
    else
    {
      sort(A.begin(), A.end());
      int count = 1;
      for (int i = 0; i < N - 1; i++)
      {
        if (A[i + 1] != A[i])
        {
          count++;
        }
      }
      return count;
    }
  }
};

template <>
class Distinct<6>
{
public:
  int solution(std::vector<int> &A)
  {
    if (int(A.size()) == 0)
      return 0;
    std::sort(A.begin(), A.end());
<<<<<<< HEAD
    
    int count=1;
    for (int i=1;i<int(A.size());i++)
=======
    bool is_new = false;
    int count = 1;
    for (int i = 1; i < int(A.size()); i++)
>>>>>>> 9e712d134313a8a81dc5a84837e215ff1037cd61
    {
      if (A[i] != A[i - 1])
        count++;
    }
    return count;
  }
};

template <>
class Distinct<16>
{
public:
  int solution(std::vector<int> &A)
  {
    std::map<int, bool> m;
    int N = A.size();

    for (int index = 0; index < N; index++)
    {
      m[A[index]] = true;
    }
    return m.size();
  }
};

template <>
class Distinct<11>
{
public:
  int solution(std::vector<int> input_array)
  {
    int array_size = input_array.size();
    if (array_size == 0)
    {
      return 0;
    }
    else
    {
      int distinct_value = 1;
      std::vector<int> sorted_input_array;
      sorted_input_array = input_array;
      std::sort(sorted_input_array.begin(), sorted_input_array.end());
      for (int index = 0; index < array_size - 1; index++)
      {
        if (sorted_input_array[index] != sorted_input_array[index + 1])
        {
          distinct_value++;
        }
      }
      return distinct_value;
    }
  }
};

} // namespace Codility
#endif
