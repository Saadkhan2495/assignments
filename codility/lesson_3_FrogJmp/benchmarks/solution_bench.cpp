#include "solutions.h"

#include <benchmark/benchmark.h>
#include <limits>

template <int32_t S>
void BM_Solution(benchmark::State &state)
{
  Codility::FrogJmp<S> question;

  for (auto _ : state)
  {
    ::benchmark::DoNotOptimize(question.solution(state.range(0), state.range(1), state.range(2)));
  }
}

BENCHMARK_TEMPLATE(BM_Solution, 1)
    ->Args({1, 10000000, 2})
    ->Args({1, 1000000000, 99})
    ->Args({1, 1000000000, 1283})
    ->Args({1, 1000000000, 1})
    ->Args({1, 900000091, 3});

BENCHMARK_TEMPLATE(BM_Solution, 6)
    ->Args({1, 10000000, 2})
    ->Args({1, 1000000000, 99})
    ->Args({1, 1000000000, 1283})
    ->Args({1, 1000000000, 1})
    ->Args({1, 900000091, 3});
BENCHMARK_TEMPLATE(BM_Solution, 11)
    ->Args({1, 10000000, 2})
    ->Args({1, 1000000000, 99})
    ->Args({1, 1000000000, 1283})
    ->Args({1, 1000000000, 1})
    ->Args({1, 900000091, 3});
BENCHMARK_TEMPLATE(BM_Solution, 16)
    ->Args({1, 10000000, 2})
    ->Args({1, 1000000000, 99})
    ->Args({1, 1000000000, 1283})
    ->Args({1, 1000000000, 1})
    ->Args({1, 900000091, 3});