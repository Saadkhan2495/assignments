#include "solutions.h"
#include "gtest/gtest.h"

#include <limits>
#include <utility>

template <typename TypeName>
class SolutionTest : public testing::Test
{
public:
  TypeName question;
};

using Solutions =
    ::testing::Types<Codility::FrogJmp<1>, Codility::FrogJmp<6>, Codility::FrogJmp<16>, Codility::FrogJmp<11>>;
TYPED_TEST_SUITE(SolutionTest, Solutions);

TYPED_TEST(SolutionTest, example1)
{
  // Arrange
  int actual = -1;
  int expected = 3;

  // Act
  actual = this->question.solution(10, 85, 30);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple1)
{
  // Arrange
  int actual = -1;
  int expected = 2;

  // Act
  actual = this->question.solution(10, 30, 10);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, simple2)
{
  // Arrange
  int actual = -1;
  int expected = 49;

  // Act
  actual = this->question.solution(1, 50, 1);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, extremes_position)
{
  // Arrange
  int actual = -1;
  int expected = 0;

  // Act
  actual = this->question.solution(10, 10, 50);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_extremes_jump)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(1, 2, 1000000000);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, many_jump1)
{
  // Arrange
  int actual = -1;
  int expected = 499995;

  // Act
  actual = this->question.solution(10, 1000000, 2);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, many_jumps2)
{
  // Arrange
  int actual = -1;
  int expected = 1;

  // Act
  actual = this->question.solution(1, 99, 99999);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, many_jumps3)
{
  // Arrange
  int actual = -1;
  int expected = 10000;

  // Act
  actual = this->question.solution(1, 12830000, 1283);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, big_extreme_jump)
{
  // Arrange
  int actual = -1;
  int expected = 1000;

  // Act
  actual = this->question.solution(1, 1000000000, 1000000);

  // Assert
  EXPECT_EQ(actual, expected);
}

TYPED_TEST(SolutionTest, small_jumps)
{
  // Arrange
  int actual = -1;
  int expected = 999999999;

  // Act
  actual = this->question.solution(1, 1000000000, 1);

  // Assert
  EXPECT_EQ(actual, expected);
}
