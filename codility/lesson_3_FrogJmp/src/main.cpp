
#include "solutions.h"

#include <iostream>

int main(void)
{
  int result = -1;
  Codility::FrogJmp<1> sundas;
  Codility::FrogJmp<6> sarosh;
  Codility::FrogJmp<11> saad;
  Codility::FrogJmp<16> bilal;
  result = sundas.solution(10, 85, 30);
  std::cout << "sundas result: " << result << std::endl;

  result = sarosh.solution(10, 85, 30);
  std::cout << "sarosh result: " << result << std::endl;

  result = saad.solution(10, 85, 30);
  std::cout << "saad result: " << result << std::endl;

  result = bilal.solution(10, 85, 30);
  std::cout << "bilal result: " << result << std::endl;

  return 0;
}
