#ifndef _SOLUTIONS_H_
#define _SOLUTIONS_H_

#include <cstdint>
#include <limits>
#include <cmath>

namespace Codility
{

template <int32_t S = 1>
class FrogJmp
{
public:
  int solution(int X, int Y, int D)
  {
    int n = ceil((double)(Y - X) / D);
    return n;
  }
};

template <>
class FrogJmp<11>
{
public:
  int solution(uint32_t start_point, uint32_t end_point, uint32_t step_size)
  {
    uint32_t distance = end_point - start_point;
    if (distance % step_size != 0)
    {
      return (distance / step_size + 1);
    }
    else
    {
      return distance / step_size;
    }
  }
};

template <>
class FrogJmp<6>
{
public:
  int solution(int X, int Y, int D)
  {
    //added 0.0 so that answers are not whole-numbers
    return ceil((Y - X + 0.0) / D);
  }
};

template <>
class FrogJmp<16>
{
public:
  int solution(int X, int Y, int D)
  {
    int jumps = ceil((Y - X) / (D * 1.0));
    return jumps;
  }
};

} // namespace Codility
#endif
